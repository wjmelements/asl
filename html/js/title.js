/*
 * Modifies the title of the document as the page changes states
 */

function useFormTitle() {
    $('title#page-title').html('meerchat');
}
function useSearchingTitle() {
    $('title#page-title').html('-*meerchat*-');
}
function useConnectingTitle() {
    $('title#page-title').html('*meerchat*');
}
function useMessengerTitle() {
    $('title#page-title').html('meerchat');
}
function useNotificationTitle() {
    // TODO oscillate
    $('title#page-title').html('*Meerchat*');
}
function useDisconnectedTitle() {
    $('title#page-title').html('/meerchat/');
}

/*
 * Modifies the title of a chat
 */
function tabNotify(tab) {
    tab.find('.partner-sides').text('*');
    tab.addClass('tab-notify');
}
function tabNormalize(tab) {
    tab.find('.partner-sides').text('');
    tab.removeClass('tab-notify');
}
function tabDisconnected(tab) {
    tab.find('.partner-sides').text('');
    tab.addClass('tab-disconnected');
}
