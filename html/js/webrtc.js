// the connection with the data channel
var peerConnection;
var caller = true;
var channel;
var offer;
var offerConstraints = {};
var toSend = {};
var toAnswer = {};
var connected = false;
var sent = false;
var answered = false;
var haveRelay = false;

function onError(err) {
    console.log(err);
}

function onLocalDescriptionSet() {
    // console.log("onLocalDescriptionSet");
}

function onOfferCreated(offer) {
    peerConnection.setLocalDescription(offer, onLocalDescriptionSet, onError);
    toSend.offer = offer;
    maybeSend();
}

function onAnswerResponse(data, textStatus, jqXHR) {
    var dataObj = JSON.parse(data);
    if ("message" in dataObj) {
        uiStopSearching();
        if (dataObj["canceled"]) {
            // TODO verify this properly requeues
            requeue();
        } else {
            document.getElementById("input_message").innerHTML+= "Error: " + dataObj["message"];
            uiShowForm();
        }
    }
}

function maybeAnswer() {
    if (!answered && toAnswer.answer && toAnswer.candidates.length > 0 && haveRelay) {
        answered = true;
        peerConnection.onicecandidate = undefined;
        var sendMessage = createMessage(toAnswer);
        $.post("answer.php", sendMessage, onAnswerResponse);
    }
}

function onPairServerResponse(data, textStatus, jqXHR) {
    var dataObj = JSON.parse(data);
    if ("message" in dataObj) {
        if (!dataObj["canceled"]) {
            document.getElementById("input_message").innerHTML+= "Error: " + dataObj["message"];
            uiStopSearching();
            uiShowForm();
        } else {
            // if the user canceled their own request then we get this reply from the pair server
            // the UI changes happen after response from cancel.php, so we shouldn't make them here
        }
        return;
    }
    useConnectingTitle();
    var messenger = prepMessenger(dataObj);
    if (dataObj.obj.answer) {
        console.log("I am the caller");
        // We initiated the caller
        // We are receiving an answer
        peerConnection.setRemoteDescription(
            new RTCSessionDescription(dataObj.obj.answer),
            // success function
            function() {},
            //  error function
            function(error) {
                console.log(JSON.stringify(error));
            }
        );
        for (var index in dataObj.obj.candidates) {
            peerConnection.addIceCandidate(
                new RTCIceCandidate(dataObj.obj.candidates[index])
            );
        }
        messenger.connection = new MessengerDataChannel(
            channel,
            peerConnection,
            messenger
        );
        prepQueue();
    } else {
        console.log("I am the callee");
        // TODO disable cancel in this step
        caller = false;
        // We need to answer
        toAnswer = {};
        toAnswer.candidates = [];
        constructPeerConnection();
        peerConnection.mcData = data;
        peerConnection.ondatachannel = function (ev) {
            channel = ev.channel;
            messenger.connection = new MessengerDataChannel(
                channel,
                peerConnection,
                messenger
            );
            prepQueue();
        }
        peerConnection.setRemoteDescription(
            new RTCSessionDescription(dataObj.obj.offer),
            // success function
            function() {
                peerConnection.createAnswer(
                    // success function
                    function (answer) {
                        peerConnection.setLocalDescription(answer, onLocalDescriptionSet, onError);
                        toAnswer.answer = answer;
                        maybeAnswer();
                    },
                    onError,
                    offerConstraints
                );
            },
            onError
        );
        for (var index in dataObj.obj.candidates) {
            peerConnection.addIceCandidate(
                new RTCIceCandidate(dataObj.obj.candidates[index])
            );
        }
    }
    if (!fileSystem) {
        startFileSystem();
    }
}

function maybeSend() {
    if (!sent && toSend.offer && toSend.candidates.length > 0 && haveRelay && psid) {
        if (!pushedButton) {
            allowSend();
            return;
        }
        sent = true;
        peerConnection.onicecandidate = undefined;
        var sendMessage = createMessage(toSend);
        $.post("send.php", sendMessage, /* successFunction */ onPairServerResponse);
        uiStartSearching();
    }
}

function createMessage(toWhatever) {
    var message = {
        id: psid,
        obj: JSON.parse(JSON.stringify(toWhatever)) // fuck you Firefox
    };
    // Lack of breaks intentional
    switch (loc) {
        case LOC_REGION:
            message['obj']['region'] = region;
        case LOC_COUNTRY:
            message['obj']['country'] = country;
    }
    if (form == "asl") {
        message['age'] = age;
        message['sex'] = sex;
        message['looking'] = looking;
        message['language'] = language;
    } else {
        message['password'] = password;
    }
    return message;
}

var servers;
function makeServerList() {
  servers = {
    iceServers: [
        // STUN https://code.google.com/p/natvpn/source/browse/trunk/stun_server_list
        createIceServer("stun:stun.l.google.com:19302"), // flaky on firefox
        createIceServer("stun:stun1.l.google.com:19302"), // flaky on firefox
        createIceServer("stun:stun2.l.google.com:19302"),
        createIceServer("stun:stun3.l.google.com:19302"),
        createIceServer("stun:stun4.l.google.com:19302"),
        // createIceServer("stun:stun01.sipphone.com"), // doesn't work on firefox
        createIceServer("stun:stun.ekiga.net"),
        // createIceServer("stun:stun.fwdnet.net"), // doesn't work on firefox
        // createIceServer("stun:stun.ideasip.com"), // flaky on firefox
        createIceServer("stun:stun.iptel.org"),
        // createIceServer("stun:stun.rixtelecom.se"), // doesn't work with firefox
        createIceServer("stun:stun.schlund.de"),
        createIceServer("stun:stunserver.org"),
        createIceServer("stun:stun.softjoys.com"),
        createIceServer("stun:stun.voiparound.com"),
        createIceServer("stun:stun.voipbuster.com"),
        createIceServer("stun:stun.voipstunt.com"),
        createIceServer("stun:stun.voxgratia.org"),
        // createIceServer("stun:stun.xten.com"), // doesn't work with firefox
        createIceServer(
            'turn:54.200.142.236:3478?transport=udp',
            'meerkat',
            'party'
        )
    ]
  }
}

var pcOptions = {
    optional: [
        {DtlsSrtpKeyAgreement:true},
    ]
};

// TURN is relay
// STUN is srflx
// HOST is host
function getCandidateType(candidateString) {
    // parse the cand-type http://www.ietf.org/rfc/rfc5245.txt
    var start = candidateString.indexOf("typ") + 4;
    var end = candidateString.indexOf(" ", start);
    return candidateString.substring(start, end);
}

function constructPeerConnection() {
    haveRelay = false;
    sent = false;
    answered = false;
    videoCandidateQueue = [];
    peerConnection = new RTCPeerConnection(servers, pcOptions);
    peerConnection.onicecandidate = function(ev) {
        if (ev.candidate) {
            var type = getCandidateType(ev.candidate.candidate);
            if (type == 'relay') {
                // so long as we have a relay option we can succeed
                haveRelay = true;
            }
            // we are either initially calling or we have found ourself the callee
            if (caller) {
                toSend.candidates.push(ev.candidate);
                maybeSend();
            } else {
                toAnswer.candidates.push(ev.candidate);
                maybeAnswer();
            }
        } else {
            // I *believe* this means no more candidates, and only gets triggered on Firefox
            if (caller) {
                if (!haveRelay) {
                    createPeerConnection();
                }
            } else {
                if (toAnswer.candidates.length == 0) {
                    document.getElementById("input_message").innerHTML += "Error: could not find ICE candidates; trying harder...";
                    // TODO verify that this works
                    onPairServerResponse(peerConnection.mcData);
                }
            }
        }
    }
}

function createPeerConnection() {
    toSend = {};
    toSend.candidates = [];
    var channelName = 'channelName';
    // http://dev.w3.org/2011/webrtc/editor/webrtc.html#idl-def-RTCDataChannelInit
    var channelOptions = {
        'negotiated': false,
        'ordered': true
    };
    constructPeerConnection();
    channel = peerConnection.createDataChannel(channelName, channelOptions);
    peerConnection.createOffer(onOfferCreated, onError, offerConstraints);
}


function MessengerDataChannel(channel, peerConnection, messenger) {
    this.channel = channel;
    this.peerConnection = peerConnection;
    this.messenger = messenger;
    this.toRemote = function(object) {
        // message should be a javascript object.
        // TODO if this.channel.readyState is not 'open' put this in a queue that sends on open
        this.channel.send(JSON.stringify(object));
    }
    this.typingToRemote = function() {
        this.toRemote({"typing": true});
    }

    this.idleToRemote = function () {
        this.toRemote({"idle": true});
    }

    this.sendToRemote = function(message) {
        this.toRemote({"text":message});
    }

    this.editToRemote = function(message, id) {
        this.toRemote({"edit":message, "id": id});
    }

    this.requestVideoToRemote = function() {
        this.toRemote({"videoRequest": true});
    }

    this.acceptedToRemote = function() {
        this.toRemote({"accepted": true});
    }

    this.rejectedToRemote = function() {
        this.toRemote({"rejected": true});
    }

    this.offerToRemote = function(offer) {
        this.toRemote({"offer": offer});
    }

    this.answerToRemote = function(answer) {
        this.toRemote({"answer": answer});
    }

    this.candidateToRemote = function(candidate) {
        this.toRemote({"candidate": candidate});
    }

    this.noteFileToRemote = function(name, size, type, width, height) {
        this.toRemote({
            "fileNote": {
                'size': size,
                'type': type,
                'width': width,
                'height': height
            },
            "name": name,
            "id": psid
        });
    }

    this.chunkFileToRemote = function(name, chunk) {
        var chunkView = Array.apply(null, new Uint32Array(chunk, 0, chunk.byteLength / 4));
        var chunkLeftover = Array.apply(null, new Uint8Array(chunk, chunk.byteLength - chunk.byteLength % 4));
        this.toRemote({"chunk": chunkView, "leftover": chunkLeftover, "id": psid, "name": name});
    }

    this.chunkRequestToRemote = function(name) {
        this.toRemote({"fileRequest": true, "name": name, "id": psid});
    }

    this.closeFileToRemote = function(name) {
        this.toRemote({"fileClose": true, "name": name, "id": psid});
    }

    this.chunkSizeToRemote = function(size) {
        this.toRemote({"chunkSize": size, "id": psid});
    }
    this.suggestYoutubeToRemote = function(youtubeID) {
        this.toRemote({"suggest": youtubeID});
    }
    this.readyYoutubeToRemote = function(youtubeID) {
        this.toRemote({"ready": youtubeID});
    }
    this.pauseToRemote = function(time) {
        // XXX does this fail for time = 0 ?
        this.toRemote({"pause": time});
    }
    this.playToRemote = function() {
        this.toRemote({"play": true});
    }
    this.closeYoutubeToRemote = function() {
        this.toRemote({"closeYoutube": true});
    }
    this.closeRemote = function() {
        // this is silly but chrome doesn't call onclose when remote closes
        this.toRemote({"close": true});
        this.channel.close();
        // this is also silly but firefox doesn't call onclose when you close
        this.channel.onclose();
    }

    this.cleanupConnections = function() {
        this.messenger.video.cleanupVideo();
    }
    this.channel.onopen = function() {
        console.log('channel opened');
        connected = true;
        localHasAcceptedVideo = false;
        remoteHasAcceptedVideo = false;
        this.messenger.start();
        this.chunkSizeToRemote(getChunkSize());
    }.bind(this);
    this.channel.onmessage = function (ev) {
        if (ev.data.length > 15000) {
            console.log('received long message size: ' + ev.data.length);
        }
        var data = JSON.parse(ev.data);
        if (data.text) {
            this.messenger.remoteSend(data.text);
        } else if (data.typing) {
            this.messenger.remoteTyping();
        } else if (data.idle) {
            this.messenger.remoteIdle();
        } else if (data.videoRequest) {
            this.messenger.video.onVideoRequested(this);
        } else if (data.accepted) {
            this.messenger.video.onRemoteAccepted(this);
        } else if (data.rejected) {
            this.messenger.video.onRemoteRejected(this);
        } else if (data.offer) {
            this.messenger.video.onVideoOfferReceived(data.offer);
        } else if (data.answer) {
            this.messenger.video.onVideoAnswerReceived(data.answer);
        } else if (data.candidate) {
            this.messenger.video.onVideoCandidateReceived(data.candidate);
        } else if (data.close) {
            this.channel.onclose();
        } else if (data.edit) {
            this.messenger.remoteEdit(data.edit, data.id);
        } else if (data.fileNote) {
            // ui note that a file is incoming and make placeholder
            this.messenger.remotePlaceholder(data.id, data.name, data.fileNote);
            // begin file transfer
            initFileFromRemote(data.id, data.name, this);
        } else if (data.chunk) {
            var buffer = new Uint32Array(data.chunk).buffer;
            // TODO do not need to send leftover over wire
            var leftover = data.leftover;
            chunkFileFromRemote(data.id, data.name, buffer, leftover, this);
        } else if (data.fileClose) {
            closeFileFromRemote(data.id, data.name, this.messenger);
        } else if (data.fileRequest) {
            chunkRequestedByRemote(data.id, data.name, this);
        } else if (data.chunkSize) {
            chunkSizeFromRemote(data.id, data.chunkSize, this);
        } else if (data.suggest) {
            this.messenger.youtubeSuggestFromRemote(data.suggest);
        } else if (data.ready) {
            this.messenger.youtubeReadyFromRemote(data.ready);
        } else if (data.pause) {
            this.messenger.youtubePauseFromRemote(data.pause);
        } else if (data.play) {
            this.messenger.youtubePlayFromRemote();
        } else if (data.closeYoutube) {
            this.messenger.$.find('.messenger-youtube-ex').click();
        } else {
            console.log('unknown incoming message');
            console.log(JSON.stringify(data));
        }
    }.bind(this);
    this.channel.onclose = function(ev) {
        console.log('channel closed');
        // TODO maybe reconnect if was dropped
        this.messenger.stop();
        this.channel.onclose = null;
        this.cleanupConnections();
        if (this.channel && this.channel.readyState == "open") {
            this.channel.close();
        }
    }.bind(this);
}
