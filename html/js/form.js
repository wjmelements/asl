/**
 * This file handles the main input form and its transition to p2p, messenger.
*/

var age = null;
var sex = null;
var looking = null;
var language = null;
var region = null;
var password = null;
var psid = null;
var region = null;
var country = null;
var pushedButton = false;
var searching = false;
var sendLocation;
var LOC_UNKNOWN = 0;
var LOC_REGION = 1;
var LOC_COUNTRY = 2;
var LOC_EARTH = 3;
var loc = LOC_UNKNOWN;

function createLanguageButton(iso, name) {
    var button = $('<span/>', {
        'class' : 'glyphicon glyphicon-remove-circle',
        value: iso
    });
    var button_wrapper = $('<div/>', {
        type: 'div',
        id: 'language-box-' + iso,
        'class': 'language-box input_button_on btn btn-sm',
        text: name,
        name: 'langs',
        value: iso
    });
    var wrapper = $('#input_languages_group');
    button.appendTo(button_wrapper);
    button_wrapper.appendTo(wrapper);
}

function updateLanguageText() {
    var buttonText;
    var languageButtons = $('.language-box');
    if (languageButtons.length == 0) {
        buttonText = 'Select';
        $('#language-text').addClass('required');
    } else {
        buttonText = languageButtons.first().text();
        if (languageButtons.length > 1) {
            buttonText += ' +' + (languageButtons.length - 1);
        }
        $('#language-text').removeClass('required');
    }
    $('#language-toggle').text(buttonText);
}

// run this before rendering page to yield correct language toggles
var selectedLanguages = {};
for (var index in probableLanguages) {
    var iso = probableLanguages[index];
    createLanguageButton(iso, window.languages[iso]['native'][0]);
    selectedLanguages[iso] = true;
}

$('#input_age').on("input", function(event) {
    verifyAge();
});

$('#language-toggle').click(function(event) {
    $('.language-view').toggle();
});

$('#input_languages_group').on("click", 'span', function(event) {
    delete selectedLanguages[($(this).attr('value'))];
    $(this).parent().remove();
    updateLanguageText();
});

$(".sex-button").on("click", "input", function(event) {
    event.stopPropagation();
    if ($(this).hasClass("checked")) {
        return;
    }
    $(this).addClass("checked");
    if ($(this).hasClass("male")) {
        $(".female.sex").removeClass("checked");
    } else {
        $(".male.sex").removeClass("checked");
    }
    $('#sex-text').removeClass('required');
});

$(".looking-button").on("click", "input", function(event) {
    event.stopPropagation();
    $(this).toggleClass("checked");
    verifyLooking();
});

function verifyAge() {
    age = document.getElementById("input_age").value;
    if (isNaN(age) || age < 14 || age > 120) {
        $('#age-text').addClass('required');
    } else{
        $('#age-text').removeClass('required');
    }
}

function verifyLooking() {
    looking = "";
    if ($(".male.looking").hasClass("checked")) {
        looking += "m";
    }
    if ($(".female.looking").hasClass("checked")) {
        looking += "f";
    }
    if (looking == "") {
        $('#looking-text').addClass('required');
    } else {
        $('#looking-text').removeClass('required');
    }
}

function verifyPassword() {
  password = $('#input-password').val();
  if (password.length == 0 || password.length > 255) {
      $('#password-text').addClass('required');
  } else {
      $('#password-text').removeClass('required');
  }
}

function genPassword() {
  $.post("pwgen.php", function(data) {
    $('#input-password').val(data);
    verifyPassword();
  });
}

function visibilityChange() {
    useMessengerTitle();
}

$(document).ready(function() {
    $('#input-password').blur(verifyPassword);
    if (!RTCPeerConnection) {
        $('button#asl_search').replaceWith('<div class="alert alert-danger" id="javascript_message">This site requires a browser with <a href="http://en.wikipedia.org/wiki/WebRTC#Support">WebRTC support</a>.</div>');
        // we should delete the share location row to remove the loading animation
        $('.share-location-row').remove();
        return;
    }
    // use post request because not nullipotent
    $.post("id.php", null, /* successFunction */ handleIDReply);
    useFormTitle();
    setupTypeahead();
    $(window).bind("beforeunload", function() {
        if (searching) {
            cancelRequest({closing: true});
        }
    });
    
    loc = LOC_REGION;
    $('.input_location').click(
        function(ev) {
            switch (loc) {
                case LOC_REGION:
                    loc = LOC_COUNTRY;
                    $('.input_location').text(country);
                    break;
                case LOC_COUNTRY:
                    loc = LOC_EARTH;
                    $('.input_location').text('Earth');
                    break;
                case LOC_EARTH:
                    loc = LOC_REGION;
                    $('.input_location').text(region);
                    break;
            }
            // $('.input_location').toggleClass('input_button_off');
            // $('.input_location').toggleClass('input_button_on');
        }
    ); // TODO button toggle for location
    makeServerList();
    document.addEventListener("visibilitychange", visibilityChange, false);
});

/*
 * Prepare for the next queue
 */
function prepQueue() {
    caller = true;
    pushedButton = false;
    createPeerConnection();
}

function queue() {
    verifyInput();
}


function languageMatcher(query, callback) {
    query = query.toLowerCase();
    var matches = [];
    for (var iso in window.languages) {
        if (iso in selectedLanguages) {
            continue;
        }
        var isFound = false;
        for (var index = 0; index < window.languages[iso]['native'].length; index++) {
            var language = window.languages[iso]['native'][index];
            var lowerLanguage = language.toLowerCase();
            if (lowerLanguage.indexOf(query) === 0) {
                matches.push({'value': language, 'iso': iso});
                isFound = true;
                break;
            }
        }
        if (isFound) {
            continue;
        }
        for (var index = 0; index < window.languages[iso]['english'].length; index++) {
            var language = window.languages[iso]['english'][index];
            var lowerLanguage = language.toLowerCase();
            if (lowerLanguage.indexOf(query) === 0) { 
                matches.push({'value': language, 'iso': iso});
            }
        }
    }
    callback(matches);
}

function onLanguageSelected(ev, language) {
    createLanguageButton(language.iso, language.value);
    updateLanguageText();
    selectedLanguages[language.iso] = true;
    clearTypeahead();
}

function setupTypeahead() {
    $('#input_languages').typeahead({
        hint: true,
        highlight: true,
        minLength: 1
    },{
        name: 'languages',
        displayKey: 'value',
        source: languageMatcher,
    }).on(
        'typeahead:selected',
        onLanguageSelected
    ).on(
        'typeahead:autocompleted',
        onLanguageSelected
    ).click(
        function(event) {
            this.setSelectionRange(0, $(this).val().length);
    });
}

function clearTypeahead() {
    $('#input_languages').typeahead('val','');
}

function uiStopSearching() {
    searching = false;
    transitionToPlus();
}

function constructChatTab(id) {
    var partner = $('<div/>', {
        id: 'tab-'+id,
        'class': 'btn-group chattab-wrapper' 
    });
    // TODO replace Searching with image
    var tab = $('<a/>', {
        'class': 'btn-chattab',
    });
    var exIcon = $('<span/>', {
      'class' : 'glyphicon glyphicon-plus-sign ex-button'
    });
    var exButton = $('<a/>', {
        'class': 'btn-chattab-ex'
    }).append(exIcon).hide();
    var partnerAge = $('<span/>', {
        'class': 'partner-age'
    });
    var partnerSex = $('<span/>', {
        'class': 'partner-sex',
    });
    var partnerLocation = $('<span/>', {
        'class': 'partner-location',
        text: 'Searching'
    });
    var textWrapper = $('<div/>', {
        'class': 'partner-text-wrapper'
    });
    var surrounding = $('<span/>', {
        'class': 'partner-sides'
    });
    textWrapper.append(surrounding)
        .append(partnerAge)
        .append("\n")
        .append(partnerSex)
        .append("\n")
        .append(partnerLocation)
        .append(surrounding.clone());
    tab.append(textWrapper);
    partner.append(tab)
        .append(exButton);
    return partner;
}

function constructChatTabWrapper() {
    var wrapper = $('<div/>', {
      'class' : 'hidden',
      'id' : 'btn-chattab-wrapper'
    });

    var plusWrapper = $('<div/>', {
      'class' : 'btn-group',
      'id' : 'btn-chattab-plus-wrapper'
    });

    var plusButton = $('<button/>', {
      type: 'button',
      'class': 'btn btn-default btn-chattab',
      'id' : 'btn-chattab-plus'
    });

    var plusIcon = $('<span/>', {
      'class' : 'glyphicon glyphicon-plus-sign'
    });

    plusButton.append(plusIcon);
    plusWrapper.append(plusButton);
    wrapper.append(plusWrapper);
    return wrapper;
}

function uiStartSearching() {
    var tab = constructChatTab(messengers.length);
    $('#btn-chattab-plus-wrapper').before(tab);
    tab.find('.partner-location').textEffect({
        effect: 'grow',
        effectSpeed: 10,
        completionSpeed: 500,
    });

    // FIXME this is a hack with the hidden class
    // essentially, the tab is only hidden this way at the beginning
    $('#btn-chattab-wrapper').removeClass('hidden').show();
    searching = true;
    transitionToCancel();
    $('#Form').hide();
    useSearchingTitle();
}

function uiShowForm() {
    $('#btn-chattab-wrapper').hide();
    $('#Form').show();
    useFormTitle();
}

function onCancelResponse(data, textStatus, jqXHR) {
    var dataObj = JSON.parse(data);
    if (!dataObj["message"]) {
        var tab = $('#tab-'+messengers.length);
        /**
         * TODO move the animation to happen when they *click* cancel
         * When we get reply, the tab should remove
         */
        tab.find(".partner-location"
        ).textEffect({
            effect: 'shrink',
            effectSpeed: 10,
            completionSpeed: 400,
            onFinish: function(elem) {
                tab.remove();
                onTabClose();
            }
        });
        sent = false;
        answered = false;
        pushedButton = false;
    }
    // do not show form if did not cancel because probably in messenger
}

function cancelRequest(data) {
    var sendMessage = createMessage({});
    // FIXME we would like to not do this synchronously on exit; make sure any changes work in chrome, firefox
    $.ajax({
        type: 'POST',
        url: 'cancel.php',
        data: sendMessage,
        async: !data.closing,
        success: onCancelResponse,
    });
    if (!data.closing) {
        uiStopSearching();
    }
}

function allowSend() {
    $('#asl_search').prop('disabled', false).empty().text('Search');
}

function disallowSend() {
    $('#asl_search').prop('disabled', true).empty().append(
        $('<img/>', {
            'class':"search-placeholder",
            src:"img/peer_typing_alpha22.gif"
        })
    );
}

function handleIDReply(data, textStatus, jqXHR) {
    var obj = JSON.parse(data);
    if ("message" in obj) {
        console.log('Error: ' + obj["message"]);
        document.getElementById("input_message").innerHTML+="Error: " + obj["message"];
        return;
    }
    psid = obj['id'];
    region = obj['region'];
    country = obj['country'];
    $('.location-placeholder').remove();
    $('.input_location').text(region);
    for (var index = 0; index < obj.turn.uris.length; index++) {
        var uri = obj.turn.uris[index];
        servers.iceServers.push(
            createIceServer(
                uri,
                obj.turn.username,
                obj.turn.password
            )
        );
    }
    // have to do this here because do not have servers yet
    createPeerConnection();
}

function passwordForm() {
    if (form == "password") {
        return;
    }
    form = "password";
    $('#asl-form').hide();
    $('#password-form').show();
    $('#password-form-tab').addClass('selected-tab');
    $('#asl-form-tab').removeClass('selected-tab');
}

function aslForm() {
    if (form == "asl") {
        return;
    }
    form = "asl";
    $('#password-form').hide();
    $('#asl-form').show();
    $('#asl-form-tab').addClass('selected-tab');
    $('#password-form-tab').removeClass('selected-tab');
}

// run before show anything
if (form === "asl") {
    form = "password";
    aslForm();
} else {
    form = "asl";
    passwordForm();
}

function verifyInput() {
    if (form == "asl") {
        verifyAge();
        sex = "";
        if ($(".male.sex").hasClass("checked")) {
            sex = "m";
        } else if ($(".female.sex").hasClass("checked")) {
            sex = "f";
        }
        verifyLooking();
        language = "";
        for (var iso in selectedLanguages) {
            if (selectedLanguages[iso]) {
                language += iso;
            }
        }
        var message = document.getElementById("input_message");
        message.innerHTML = "";
        if (sex == "") {
            $('#sex-text').addClass('required');
        } else{
            $('#sex-text').removeClass('required');
        }
        // languages is an isocode string so if it's odd it's wrong
        if (language.length == 0 || language.length % 2 == 1) {
            $('#language-text').addClass('required');
        } else {
            $('#language-text').removeClass('required');
        }
    } else {
        verifyPassword();
    }
    if ($('div#main_table .required').length == 0) {
        if (!pushedButton) {
            pushedButton = true;
            maybeSend();
        }
    }
}
