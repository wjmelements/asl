// the connection with the audio/video
Video.prototype.videoPeerConnection;
Video.prototype.videoOfferConstraints = {};
// FIXME protect against race conditions where both request at similar times
// TODO replicate these for each messenger
Video.prototype.videoCaller = true;
Video.prototype.gotVideo = false;
Video.prototype.localHasAcceptedVideo = false; // has three states (true, false, 'rejected')
Video.prototype.remoteHasAcceptedVideo = false; // has three states (true, false, 'rejected')
Video.prototype.localStream;
Video.prototype.localVideo;
Video.prototype.remoteVideo;
Video.prototype.videoOffer = null;
Video.prototype.messenger;
// FIXME this is a hack; when we know how to do video, don't use this variable

function Video(messenger) {
    this.messenger = messenger;
}

Video.prototype.videoOfferReady = function(offer) {
    this.videoOffer = offer;
    this.maybeVideoOffer();
}

Video.prototype.maybeVideoOffer = function() {
    // send offer if we received accept and if we have created the offer
    if (this.videoOffer && this.remoteHasAcceptedVideo === true) {
        this.messenger.connection.offerToRemote(this.videoOffer);
    } else if (this.remoteHasAcceptedVideo === 'rejected') {
        this.cleanupVideo();
        this.remoteHasAcceptedVideo = false;
    }
}

Video.prototype.videoAnswerReady = function(answer) {
    this.messenger.connection.answerToRemote(answer);
    this.displayVideo();
}

Video.prototype.onVideoRequested = function() {
    var messenger = this.messenger;
    messenger.$.find('.video-btn').hide();
    this.videoCaller = false;
    messenger.$.find('div.request-video').show();
    // TODO unregister these events after close so they don't get called twice
    messenger.$.find('.request-video-accept').click(this.onVideoAccepted.bind(this));
    messenger.$.find('.request-video-reject').click(this.onVideoRejected.bind(this));
}

Video.prototype.onRemoteRejected = function() {
    var messenger = this.messenger;
    this.remoteHasAcceptedVideo = 'rejected';
    // TODO know whether to do this or not and don't cleanup more than once
    this.cleanupVideo();
    // wait a while before you can request again
    var timeoutID = setTimeout(
        function() {
            this.remoteHasAcceptedVideo = false;
            this.videoCaller = true;
            // TODO clear this timer if other video events happen
            messenger.$.find('.video-btn').show();
            clearTimeout(timeoutID);
        }.bind(this), 10000 // milliseconds
        // TODO show this timer to the user?
    );
}

// you have accepted a video offer
Video.prototype.onVideoAccepted = function() {
    var messenger = this.messenger;
    messenger.$.find('.request-video').hide();
    this.localHasAcceptedVideo = true;
    this.messenger.connection.acceptedToRemote();
}

Video.prototype.onRemoteAccepted = function() {
    this.remoteHasAcceptedVideo = true;
    this.maybeVideoOffer();
}

// you have rejected a video offer
Video.prototype.onVideoRejected = function() {
    var messenger = this.messenger;
    messenger.$.find('.request-video').hide();
    messenger.connection.rejectedToRemote();
    this.videoCaller = true;
    messenger.$.find('.video-btn').show();
}

Video.prototype.onVideoOfferReceived = function(offer) {
    // if not, did not grant permission; this should not happen unless the peer is trying to hack the system
    if (this.localHasAcceptedVideo) {
        this.onVideoOfferReceivedAndAccepted(offer);
    }
}

Video.prototype.onVideoOfferReceivedAndAccepted = function(offer) {
    this.videoCaller = false;
    this.constructVideoPeerConnection();
    this.videoPeerConnection.setRemoteDescription(new RTCSessionDescription(offer),
        // success function
        this.maybeVideoCreateAnswer.bind(this),
        //  error function
        function(error) {
            console.log(JSON.stringify(error));
        }
    );
}

Video.prototype.videoCandidateQueue = [];
Video.prototype.onVideoCandidateReceived = function(candidate) {
    if (this.videoPeerConnection && this.videoPeerConnection.remoteDescription) {
        this.videoPeerConnection.addIceCandidate(new RTCIceCandidate(candidate));
    } else {
        // put in queue for later
        this.videoCandidateQueue.push(candidate);
    }
}

Video.prototype.onVideoAnswerReceived = function(answer) {
    this.videoPeerConnection.setRemoteDescription(new RTCSessionDescription(answer),
        // success function
        function() {
            for (var index = 0; index < this.videoCandidateQueue.length; index++) {
                consolelog('adding ice candidate');
                this.videoPeerConnection.addIceCandidate(new RTCIceCandidate(this.videoCandidateQueue[index]));
            }
            this.videoCandidateQueue.length = 0;
        }.bind(this),
        // error function
        function (ev) {
            console.log(ev);
        }
    );
    this.displayVideo();
}

// this is the entry point for adding a video connection
Video.prototype.constructVideoPeerConnection = function() {
    var messenger = this.messenger;
    this.localVideo = messenger.$.find('.messenger-local-video')[0];
    this.remoteVideo = messenger.$.find('.messenger-remote-video')[0];
    this.remoteHasAcceptedVideo = false;
    messenger.$.find('.video-btn').hide();
    messenger.$.find('div.requesting-video').show();
    if (this.videoCaller) {
        messenger.connection.requestVideoToRemote();
    }
    this.gotVideo = false;
    this.videoPeerConnection = new RTCPeerConnection(servers, pcOptions);
    this.videoPeerConnection.onicecandidate = function(ev) {
        if (ev.candidate) {
            console.log("oncidecandidate:candidate found");
            messenger.connection.candidateToRemote(ev.candidate);
        } else {
            // I believe this means that there aren't any more ice candidates to try
            console.log("onicecandidate: nope");
            //document.getElementById("input_message").innerHTML+="Error: could not find ICE candidates";
        }
    };
    this.videoPeerConnection.onaddstream = function(ev) {
        this.remoteStream = ev.stream;
        attachMediaStream(this.remoteVideo, ev.stream);
    }.bind(this);
    this.videoPeerConnection.onopen = function() { console.log('onopen'); }
    this.videoPeerConnection.onconnecting = function() { console.log('onconnecting'); }
    this.startVideo();
}

Video.prototype.displayVideo = function() {
    var messenger = this.messenger;
    messenger.$.find('div.requesting-video').hide();
    messenger.$.find('video.messenger-remote-video').show();
    messenger.$.find('video.messenger-local-video').show()
    messenger.top_offset = 750;
    messenger.updateMessageHeight();
}

Video.prototype.startVideo = function() {
    var mediaConstraints = {
        audio: true,
        video: true
    };
    getUserMedia(
        mediaConstraints,
        // success function
        function (stream) {
            var messenger = this.messenger;
            messenger.$.find('div.requesting-video').hide();
            this.localStream = stream;
            // FIXME are the next two lines redundant?
            attachMediaStream(this.localVideo, stream);
            this.videoPeerConnection.addStream(stream);
            this.gotVideo = true;
            if (this.videoCaller) {
                // caller
                this.videoPeerConnection.createOffer(this.onVideoOfferCreated.bind(this), onError, this.videoOfferConstraints);
            } else {
                // callee
                this.maybeVideoCreateAnswer();
            }
        }.bind(this),
        // error function
        function (error) {
            console.log(error);
        }
    );
}

Video.prototype.maybeVideoCreateAnswer = function() {
    // must have gotten user media and must have set remote description
    if (this.videoPeerConnection.remoteDescription && this.gotVideo) {
        this.videoPeerConnection.createAnswer(
            // success function
            function (answer) {
                this.videoPeerConnection.setLocalDescription(answer);
                this.videoAnswerReady(answer);
            }.bind(this),
            // error function
            function (error) {
                console.log(error);
            },
            this.videoOfferConstraints
        );
        for (var index = 0; index < this.videoCandidateQueue.length; index++) {
            this.videoPeerConnection.addIceCandidate(new RTCIceCandidate(this.videoCandidateQueue[index]));
        }
        this.videoCandidateQueue.length = 0;
    }
}

Video.prototype.onVideoOfferCreated = function(offer) {
    this.videoPeerConnection.setLocalDescription(offer, onLocalDescriptionSet, onError);
    this.videoOfferReady(offer);
}

// FIXME callee requeueing after caller getUserMedia cannot make video with guy again
Video.prototype.cleanupVideo = function() {
    if (this.videoPeerConnection) {
        if (this.videoPeerConnection.signalingState != 'closed') {
            this.videoPeerConnection.close();
        }
        this.videoPeerConnection = null;
    }
    if (this.videoOffer) {
        this.videoOffer = null;
    }
    this.messenger.$.find('video').hide();
    // TODO verify this stops the webcam on Opera
    // TODO handle disconnect when in fullscreen
    if (this.localStream) {
        this.localStream.stop();
        this.localStream = null;
        this.localVideo.pause();
        this.localVideo.src="";
    }
}

