window.requestFileSystem =
    window.requestFileSystem
    || window.webkitRequestFileSystem;

window.indexedDB =
    window.indexedDB
    || window.mozIndexedDB
    || window.webkitIndexedDB;

/** Restrictions on chunkSize:
 * Array.apply has a limit of 500000 on Firefox
 * Array.apply has a lower limit on Chrome.
 * Chrome cannot send/recv data larger than 16384
 */
function calcChunkSize() {
    if (window.requestFileSystem) {
        // chrome cannot receive larger than 16384 at a time (except from chrome)
        // TODO improve view efficiency so can have larger chunk size
        return Math.floor(16384 / 3.3); // fuck you too, Chrome
    } else {
        return Math.floor(500000);
    }
}
// id -> chunkSize that can receive
var chunkSize = [];
function chunkSizeFromRemote(id, size) {
    // do not change chunksize once it is set for security
    if (!chunkSize[id]) {
        chunkSize[id] = size;
    }
}
function getChunkSize() {
    if (chunkSize[psid]) {
        return chunkSize[psid];
    } else {
        chunkSize[psid] = calcChunkSize();
        return chunkSize[psid];
    }
}

/**
 * We copy files into a temporary file system
 * from which we display them in the messenger.
 * We have this level of indirection in case a user
 * deletes or moves a file at particular points.
 * Both provided and gotten files are therefore
 * ephemeral for the user; they would have to manually
 * download them.
 * We could totally just use the file we're given
 * and save a lot of space. Only do if we find
 * the change necessary.
 */

var fileSystem;
// Map: id + name -> file entry or file handle, depending on browser
var fileMap;
// Map: name -> num chunks sent
var fileProgress;
// Map: name -> buffer
var fileBuffers;
var fsCounter = 0;

function fsError(e) {
    var msg = "FSError: " + e.name;
    if (e.message) {
        msg += " - " + e.message;
    }
    console.log(msg);
}

function startFileSystem() {
    // TODO hide file functionality when no filesystem
    if (requestFileSystem) {
        requestFileSystem(TEMPORARY, 200*1024*1024,
            // success function
            function (fs) {
                fileSystem = fs;
            },
            // error function
            /**
             * TODO what if they don't allow us that much space
             * @will has investigated this and believes that the disk quota isn't yet enforced on any platform 
             */
            fsError
        );
    } else {
        var request = indexedDB.open("fileSystem", {storage: "temporary"});
        request.onsuccess =
            function(ev) {
                fileSystem = request.result;
            };
        request.onfailure =
            function(ev) {
                // well shit
            };
    }
    fileMap = [];
    fileProgress = [];
    fileBuffers = [];
    chunkSize = [];
}
// make the original name of the file
function original_filename(name) {
    return name.replace(/\d+_/, '');
}
function makeLocalCopy(file, messenger, channel) {
    fsCounter++;
    var name = fsCounter + '_' + file.name;
    var isImage = isImageFile(name);
    if (requestFileSystem) {
        // FIXME directories to remove at clearFileSystem
        fileSystem.root.getFile(fsCounter + name, {create:true},
            // success function
            function(fileEntry) {
                fileMap[psid + name] = fileEntry;
                fileEntry.createWriter(
                    // success function
                    function(fileWriter) {
                        fileWriter.onwriteend =
                            function (e) {
                                messenger.putMessage(localFile(url, name, file.type));
                                if (isImage) {
                                    var img = document.getElementById(url);
                                    img.onload = function() {
                                        channel.noteFileToRemote(name, file.size, file.type, img.width, img.height);
                                    }
                                } else {
                                    channel.noteFileToRemote(name, file.size, file.type);
                                }
                                var reader = new FileReader();
                                reader.onloadend =
                                    function (ev) {
                                        if (ev.target.readyState == FileReader.DONE) {
                                            var buffer = this.result;
                                            fileProgress[name] = 0;
                                            fileBuffers[name] = buffer;
                                        }
                                    };
                                reader.onerror = fsError;
                                reader.onprogress // TODO
                                reader.readAsArrayBuffer(file);
                            };
                        fileWriter.write(file);
                        var url = fileEntry.toURL();
                    }
                )
            },
            // error function
            fsError
        )
    } else {
        var builder = fileSystem.mozCreateFileHandle(name, file.type);
        builder.onsuccess =
            function() {
                var handle = this.result;
                fileMap[psid + name] = handle;
                var reader = new FileReader();
                reader.onloadend =
                    function(ev) {
                        if (ev.target.readyState == FileReader.DONE) {
                            var editor = handle.open('readwrite');
                            /// XXX is the entire file in memory here? (I hope not)
                            var buffer = this.result
                            fileBuffers[name] = buffer;
                            fileProgress[name] = 0;
                            var writing = editor.write(buffer);
                            writing.onsuccess =
                                function() {
                                    // FIXME this has a bug on Firefox
                                    // https://bugzilla.mozilla.org/show_bug.cgi?id=898856
                                    var fileRequest = handle.getFile(); 
                                    fileRequest.onsuccess =
                                        function() {
                                            var f = this.result;
                                            var url = URL.createObjectURL(f);
                                            if (isImage) {
                                                var html = localImage(url);
                                                messenger.putMessage(html);
                                                var img = document.getElementById(url);
                                                img.onload = function() {
                                                    channel.noteFileToRemote(name, file.size, file.type, img.width, img.height);
                                                }
                                            } else {
                                                var html = localLink(url, name, file.type);
                                                messenger.putMessage(html);
                                                channel.noteFileToRemote(name, file.size, file.type);
                                            }
                                        };
                                    fileRequest.onerror = fsError;
                                };
                            writing.onerror = fsError;
                        }
                    };
                reader.onprogress; // TODO show progress for reading and writing
                // cannot read as data url because need to have in fileSystem
                reader.readAsArrayBuffer(file);
            };
        builder.onerror = fsError;
    }
}
function chunkRequestedByRemote(id, name, channel) {
    var progress = fileProgress[name];
    var size = Math.min(chunkSize[id], chunkSize[psid]);
    var position = progress * size;
    var buffer = fileBuffers[name];
    if (position >= buffer.byteLength) {
        // finished
        channel.closeFileToRemote(name);
    } else {
        var chunk = buffer.slice(position, Math.min(position + size, buffer.byteLength));
        fileProgress[name] = progress + 1;
        channel.chunkFileToRemote(name, chunk);
    }
}
function initFileFromRemote(id, name, channel) {
    if (requestFileSystem) {
        fileSystem.root.getFile(id + name, {create:true},
            // success function
            function(fileEntry) {
                fileMap[id + name] = fileEntry;
                channel.chunkRequestToRemote(name);
            },
            // error function
            fsError
        );
    } else {
        var builder = fileSystem.mozCreateFileHandle(id + name);
        builder.onsuccess =
            function() {
                var handle = this.result;
                fileMap[id + name] = handle;
                channel.chunkRequestToRemote(name);
            }
        builder.onerror = fsError;
    }
}
function closeFileFromRemote(id, name, messenger) {
    if (requestFileSystem) {
        var fileEntry = fileMap[id + name];
        var url = fileEntry.toURL();
        messenger.fileFrom(url, id, name);
    } else {
        var handle = fileMap[id + name];
        var fileRequest = handle.getFile();
        fileRequest.onsuccess =
            function() {
                var f = this.result;
                var url = URL.createObjectURL(f);
                messenger.fileFrom(url, id, name);
            }
        fileRequest.onerror = fsError;
    }
}

function chunkFileFromRemote(id, name, buffer, leftover, channel) {
    if (requestFileSystem) {
        var fileEntry = fileMap[id + name];
        fileEntry.createWriter(
            // success function
            function (fileWriter) {
                // start write at end of file
                fileWriter.onwriteend =
                    function (ev) {
                        if (!leftover.length) {
                            channel.chunkRequestToRemote(name);
                        } else {
                            blob = new Blob([new Uint8Array(leftover).buffer]);
                            fileWriter.onwriteend = function (ev) {
                                channel.chunkRequestToRemote(name);
                            }
                            fileWriter.write(blob);
                        }
                    };
                fileWriter.onerror = fsError;
                fileWriter.seek(fileWriter.length);
                var blob = new Blob([buffer]);
                fileWriter.write(blob);
            },
            // error function
            fsError
        );
    } else {
        var handle = fileMap[id + name];
        var editor = handle.open("readwrite");
        var writing = editor.append(buffer);
        if (leftover.length) {
            writing = editor.append(new Uint8Array(leftover).buffer);
        }
        writing.onsuccess =
            function () {
                channel.chunkRequestToRemote(name);
            };
        writing.onerror = fsError;
    }
}
function clearFileSystem(id) {
    // TODO free temporary storage for next session
}
