function makeIns(content) {
    var sp = document.createElement('span');
    sp.classList.add('messenger-ins');
    sp.innerHTML = content;
    return sp.outerHTML;
}

function makeDel(content) {
    var sp = document.createElement('span');
    sp.classList.add('messenger-del');
    sp.innerHTML = content;
    return sp.outerHTML;
}

function wordDiff(a, b) {
    var aList = wordList(a);
    var bList = wordList(b);
    var same = lcs(aList, bList);
    var result = "";
    var i = 0, j = 0, k = 0, m = aList.length,
        n = bList.length, o = same.length;
    var current = "";
    while (k < o) {
        while (aList[i] != same[k]) {
            current += aList[i++];
        }
        if (current.length) {
            result += makeDel(current);
            current = "";
        }
        while (bList[j] != same[k]) {
            current += bList[j++];
        }
        if (current.length) {
            result += makeIns(current);
            current = "";
        }
        while (k < o && aList[i] == bList[j]) {
            i++;
            j++;
            result += same[k++];
        }
    }
    while (i < m)
        current += aList[i++];
    if (current.length) {
        result += makeDel(current);
        current = '';
    }
    while (j < n)
        current += bList[j++];
    if (current.length) {
        result += makeIns(current);
    }
    return result;
        
}

function lcs(a, b) {
    if (a.length < b.length) {
        var s = a;
        a = b;
        b = s;
    }
    var m = a.length;
    var n = b.length;
    var prefix = [];
    var i = 0;
    while (i < n && a[i] == b[i])
        prefix.push(a[i++]);
    if (i == n)
        return prefix;
    var suffix = [];
    var j = 0;
    while (j < n && a[a.length - j - 1] == b[b.length - j - 1])
        suffix.push(a[a.length - (j++) - 1]);
    suffix.reverse();
    if (i + j > a.length) {
        i = a.length - j;
        prefix = prefix.slice(0, i);
    } else if (i + j > b.length) {
        i = b.length - j;
        prefix = prefix.slice(0, i);
    }
    if (i + j == n)
        return prefix.concat(suffix);
    if (j == 0) {
        a = a.slice(i);
        b = b.slice(i);
    } else {
        a = a.slice(i, -j);
        b = b.slice(i, -j);
    } 
    var middle = lcs_middle(a, b);
    return prefix.concat(middle).concat(suffix);
}

// http://rosettacode.org/wiki/Longest_common_subsequence#Dynamic_Programming_3
function lcs_middle(x,y){
    var s,i,j,m,n,
        lcs=[],row=[],c=[],
        left,diag,latch;
    m = x.length;
    n = y.length;
    //make sure shorter string is the column string
    if(m<n){s=x;x=y;y=s;s=m;m=n;n=s;}
    //build the c-table
    for(j=0;j<n;row[j++]=0);
    for(i=0;i<m;i++){
        c[i] = row = row.slice();
        for(diag=0,j=0;j<n;j++,diag=latch){
            latch=row[j];
            if(x[i] == y[j]){row[j] = diag+1;}
            else{
                left = row[j-1]||0;
                if(left>row[j]){row[j] = left;}
            }
        }
    }
    i--,j--;
    //row[j] now contains the length of the lcs
    //recover the lcs from the table
    while(i>-1&&j>-1){
        switch(c[i][j]){
            default: j--;
                     lcs.unshift(x[i]);
            case (i&&c[i-1][j]): i--;
                                 continue;
            case (j&&c[i][j-1]): j--;
        }
    }
    return lcs;
}

/* Splits a string into word and non-word components */
function wordList(str) {
    var words = str.split(/\W+/).reverse();
    var nonwords = str.split(/\w+/).reverse();
    var all = [];
    var firstWord = words.pop();
    if (firstWord) {
        all[all.length] = firstWord;
        nonwords.pop();
    }
    while (words.length > 0 || nonwords.length > 0) {
        if (nonwords) {
            var text = nonwords.pop();
            if (text)
                all[all.length] = text;
        }
        if (words) {
            var text = words.pop();
            if (text)
                all[all.length] = text;
        }
    }
    return all;
}
