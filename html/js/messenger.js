/**
 * Shows the messenger in the UI
 */
 
Messenger.prototype.top_offset = 250; 
var KEYCODE_ENTER = 13;
var KEYCODE_ESC = 27;

// Opposite plugin definition
/*$.fn.unlinkify = function () {
    this.find('a.linkified').contents().unwrap();
    return this;
};*/

function onResize() {
    var width = $(this).width();
    if (width <= 350) {
        $('div.cover-container').addClass('mobile');
    } else {
        $('.cover-container').removeClass('mobile');
    }
    if (selectedMessengerID !== undefined) {
        messengers[selectedMessengerID].updateMessageHeight();
    }
}
$(window).resize(onResize);

Messenger.prototype.updateMessageHeight = function() {
    var height = $(window).height();
    this.$.find('div.messages').height(height - this.top_offset);
}

var messengers = [];

function prepMessenger(reply) {
    var messenger = new Messenger(messengers.length);
    messengers.push(messenger);
    $('#btn-chattab-wrapper').after(messenger.$);
    if (reply['obj']['country']) {
        var partnerLocation = reply['obj']['country'];
        if (partnerLocation == country && reply['obj']['region']) {
            partnerLocation = reply['obj']['region'];
        }
        messenger.tab.find('.partner-location').text(partnerLocation);
    } else {
        messenger.tab.find('.partner-location').text('');
    }
    if (form == 'asl') {
        messenger.tab.find('.partner-age').text(reply['age']);
        messenger.tab.find('.partner-sex').text(
            reply['male'] == 1
                ? "M" 
                : "F"
        );
    }
    return messenger;
}

function transitionToCancel() {
    // TODO rotate + to X
    $('#btn-chattab-plus').unbind(
    ).click({closing: false}, cancelRequest
    );

    $('.roll-plus-button').removeClass('plus-button').addClass('ex-button');
}

function transitionToPlus() {
    // TODO rotate X to +
    $('#btn-chattab-plus').unbind(
    ).click(queue
    );
    
    $('.roll-plus-button').removeClass('ex-button').addClass('plus-button');
}

Messenger.prototype.constructVideoButton = function() {
    var videoBtn = $('<div/>', {
        'class': 'video-btn partner-btn'
    });
    var videoBtnClick = $('<a/>', {
        'class': 'tooltip-btn',
        href: '#',
        title: 'Start Video Call'
        }
    ).data('toggle', 'tooltip'
    ).data('placement', 'top'
    ).click(this, function() {
        this.video.constructVideoPeerConnection();
    }.bind(this));
    var videoBtnIcon = $('<span/>', {
        'class': 'glyphicon glyphicon-facetime-video'
    });
    videoBtnClick.append(videoBtnIcon);
    videoBtn.append(videoBtnClick);
    return videoBtn;
}

Messenger.prototype.constructDisconnectButton = function() {
    var disconnectBtn = $('<div/>', {
        'class': 'disconnect-btn partner-btn'
    });
    var disconnectBtnClick = $('<a/>', {
        'class': 'tooltip-btn',
        'href': '#',
        'title': 'Disconnect',
        }
    ).data('toggle', 'tooltip'
    ).data('placement', 'top'
    ).click(this.close.bind(this)
    );
    var disconnectBtnIcon = $('<span/>', {
        'class': 'glyphicon glyphicon-repeat'
    });
    disconnectBtnClick.append(disconnectBtnIcon);
    disconnectBtn.append(disconnectBtnClick);
    return disconnectBtn;
}

Messenger.prototype.constructTray = function () {
    var tray = $('<div/>', {
        'class': 'tray input-group'
    });
    var fileInputLabel = $('<label/>', {
        'class': 'file-input-label'
    });
    var videoButton = this.constructVideoButton();
    var messengerTextInput = $('<textarea/>', {
        'class': 'messenger-text-input form-control'
    }).keypress(this.onMessengerTyped.bind(this)
    ).focus(function () {
        tabNormalize(this.tab);
        fileInputLabel.hide();
        videoButton.hide();
    }.bind(this)
    ).blur(function () {
        if ($(this).val() == '') {
            fileInputLabel.show();
            videoButton.show();
        } else {
            fileInputLabel.hide();
            videoButton.hide();
        }
    }
    ).val(''
    );
    var messengerFileInput = $('<input/>', {
        type: 'file',
        'class': 'messenger-file-input'
    }).attr('multiple', '').change(this.localFiles.bind(this));
    var messengerFileIcon = $('<span/>', {
        'class': 'glyphicon glyphicon-paperclip'
    });

    fileInputLabel.append(messengerFileIcon);
    fileInputLabel.append(messengerFileInput);
    tray.append(messengerTextInput);
    tray.append(videoButton);
    tray.append(fileInputLabel);
    return tray;
}

Messenger.prototype.constructYoutubeVideo = function () {
    var youtubeVideo = $('<div/>', {
        id: this.getYoutubeVideoID(),
        'class': 'messenger-youtube',
    }).hide();
    return youtubeVideo;
}

Messenger.prototype.constructMessagesWrapper = function () {
    var messagesWrapper = $('<div/>', {
        'class': 'messages-wrapper'
    });
    var requestVideo = $('<div/>', {
        'class': 'alert alert-warning request-video',
        role: 'alert'
    }).hide(
    ).text('Requesting video, do you want to accept?'
    );
    var requestVideoAccept = $('<button/>', {
        type: 'button',
        'class': 'request-video-accept btn',
    }).text('Yes');
    var requestVideoReject = $('<button/>', {
        type: 'button',
        'class': 'request-video-reject btn'
    }).text('No');
    var requestingVideo = $('<div/>', {
        'class': 'requesting-video alert alert-warning',
        role: 'alert',
    }).hide().text('Please allow the video feed to continue');
    var messages = $('<div/>', {
        'class': 'messages'
    });
    var remoteVideo = $('<video/>', {
        'class': 'messenger-remote-video'
    }).attr('autoplay', ''
    ).hide(
    ).click(function() {
        if($(this)[0].webkitRequestFullScreen) {
            $(this)[0].webkitRequestFullScreen();
        } else if ($(this)[0].mozRequestFullScreen) {
            $(this)[0].mozRequestFullScreen();
        } else {
            console.log("cannot request full screen");
        }
    });
    var localVideo = $('<video/>', {
        'class': 'messenger-local-video'
    }).attr('autoplay', '').prop('muted', true).hide();
    var youtubeVideo = this.constructYoutubeVideo();
    var youtubeEx = $('<button/>', {
        type: 'button',
        'class': 'btn messenger-youtube-ex',
    }).click(makeOnYoutubeEx(this.id)
    ).append(
        $('<span/>', {
            'class': 'glyphicon glyphicon-plus-sign ex-button',
        })
    ).hide(
    );
    var tray = this.constructTray();
    requestVideo.append(requestVideoAccept);
    requestVideo.append(requestVideoReject);
    messagesWrapper.append(requestVideo)
    .append(requestingVideo)
    .append(remoteVideo)
    .append(localVideo)
    .append(youtubeVideo)
    .append(youtubeEx)
    .append(messages)
    .append(tray);
    return messagesWrapper;
}

Messenger.prototype.getYoutubeVideoID = function() {
    return 'messenger-youtube-'+this.id;
}

Messenger.prototype.youtubeSuggestFromRemote = function(youtubeID) {
    this.$.find('.suggested-remote.suggested-'+youtubeID).addClass('checked');
    maybeStartWatching(this.id, youtubeID);
}

Messenger.prototype.youtubeReadyFromRemote = function(videoID) {
    onRemoteReady(this.player);
}

Messenger.prototype.youtubePauseFromRemote = function(time) {
    onRemotePause(this.player, time)
}

Messenger.prototype.youtubePlayFromRemote = function() {
    onRemotePlay(this.player);
}

var selectedMessengerID;

function getSelectedMessenger() {
    return messengers[selectedMessengerID];
}

Messenger.prototype.makeSelectedMessenger = function() {
    selectedMessengerID = this.id;
    $('.messenger-wrapper').hide();
    this.$.show();
    $('#btn-chattab-wrapper > div.chattab-selected').removeClass('chattab-selected');
    this.tab.addClass('chattab-selected');
    this.updateMessageHeight();
}

// class Messenger
function Messenger(id) {
    this.id = id;
    var messengerWrapper = $('<div/>', {
        id: id,
        'class': "messenger-wrapper"
    }).hide();
    var messenger = $('<div/>', {
        'class': 'messenger'
    });
    var messagesWrapper = this.constructMessagesWrapper();
    messenger.append(messagesWrapper);
    messengerWrapper.append(messenger);
    this.tab = $('#tab-'+id);
    this.video = new Video(this);
    this.$ = messengerWrapper;
    this.unload = function() {
        if (connected) {
            this.connection.closeRemote();
        }
    }.bind(this);
    $(window).bind("beforeunload", this.unload);
    // TODO connecting message
}

Messenger.prototype.partner = null;

// do not call onTabClose on requeue
// find a tab to make selected, or show the form again
function onTabClose() {
    if ($('.btn-chattab-ex').length == 0) {
        uiShowForm();
    } else {
        if ($('#' + selectedMessengerID + '.messenger-wrapper').length) {
            // the tab closed was not the selected messenger
            return;
        }
        // TODO optimize this tab-finding logic
        var wrappers = $('.messenger-wrapper');
        if (wrappers.length > 0) {
            var lastID = wrappers[wrappers.length- 1].id;
            if (lastID < selectedMessengerID) {
                messengers[lastID].makeSelectedMessenger();
            }
            for (var index = 0; index < wrappers.length; index++) {
                var id = wrappers[index].id;
                if (id > selectedMessengerID) {
                    messengers[id].makeSelectedMessenger();
                    return;
                }
            }
        }
    }
}

Messenger.prototype.start = function() {
    // TODO connected message
    uiStopSearching();
    $('.messenger-wrapper').hide();
    this.$.show();
    this.tab.find('.btn-chattab-ex').show(
    ).click(function (ev) {
        this.connection.closeRemote();
        this.destroy();
        onTabClose();
    }.bind(this));
    // TODO do we need these?
    this.$.find('.partner-btn').show();
    this.$.find('div.tray').show();

    this.tab.click(function(ev) {
        this.makeSelectedMessenger();
        tabNormalize(this.tab);
    }.bind(this)).addClass('clickable');
    useMessengerTitle();
    this.makeSelectedMessenger();
    this.updateMessageHeight();

    idleTimer = setInterval(this.onTimeInterval.bind(this), 1000);
}

/*
 * Called when the user presses disconnect
 */
Messenger.prototype.close = function() {
    this.connection.closeRemote();
    // TODO (js-optimization) do not need to do complicated things to UI here
    this.requeue();
}

Messenger.prototype.constructDisconnectMessage = function () {
    var wrapper = $('<div/>');
    var button = $('<button/>', {
        type: 'button',
        'class': 'btn messenger-again'
    }).click(this.requeue.bind(this)
    ).text('Search Again');
    wrapper.append(button);
    return wrapper;
}

/*
 * Called when either side disconnects
 */
Messenger.prototype.stop = function() {
    this.$.find('.partner-btn').hide();
    // TODO is it better to disable here?
    var tray = this.$.find('div.tray');
    this.top_offset -= 35;// TODO get actual tray height (it can be hidden)
    this.updateMessageHeight();
    tray.hide();
    this.putMessage(
        "<div class='disconnect-wrapper'><span class='messenger-disconnnected'>" +
            "Tunnel has collapsed." +
        "</span></div>"
    );
    this.tab.find('.btn-chattab-ex').unbind(
    ).click(function (ev) {
        this.destroy();
        onTabClose();
    }.bind(this));

    this.putMessage(this.constructDisconnectMessage());
    this.$.find('.messages-wrapper').css({'background-color': '#777', 'transition': 'background-color .6s'});
    clearFileSystem();
    clearInterval(idleTimer);
    $(window).unbind("beforeunload", this.unload);
    tabDisconnected(this.tab);
}

Messenger.prototype.requeue = function() {
    this.destroy();
    queue();
}

/**
 * Eradicates the messenger
 */
Messenger.prototype.destroy = function() {
    this.tab.remove();
    this.$.remove();
}


var messageCounts = {'You': 0, 'Peer': 0};

function fileMessageId(id, name) {
    return 'message-' + id + '-' + name;
}
function filePlaceholderId(id, name) {
    return fileMessageId(id, name) + '-span';
}
function imagePlaceholder(user, id, name, fileNote) {
    var item = $('<div/>', {
        'id': fileMessageId(id, name),
        'class': 'messenger-item'
    });
    var message = $('<canvas/>', {
        'id': filePlaceholderId(id, name),
        'class': 'messenger-message messenger-file messenger-placeholder message-from-' + user,
        'width': fileNote.width,
        'height': fileNote.height
    });
    message.appendTo(item);
    return item;
}

function filePlaceholder(user, id, name) {
    var item = $('<div/>', {
        'id': fileMessageId(id, name),
        'class': 'messenger-item'
    });
    // TODO better file link placeholder
    var message = $('<span/>', {
        'id': filePlaceholderId(id, name),
        'class': 'messenger-message messenger-file messenger-placeholder message-from-' + user
    });

    var img = $('<img/>', {
        'src': 'img/peer_typing_alpha22.gif'
    });
    img.appendTo(message);
    message.appendTo(item);
    return item;
}

Messenger.prototype.putMessageFrom = function(user, text) {
    var count = (messageCounts[user] += 1);
    var item = $('<div/>', {
        'class': 'messenger-item'
    });
    var message = $('<span/>', {
        'id': 'message-' + user + '-' + count,
        'class': 'messenger-message message-from-' + user,
        'text': text
    });
    // input is sanitized, line breaks must bi enabled manually
    message.html(message.html().replace(/\n/g, '<br>')); 
    message.appendTo(item);
    if (user == 'You') {
        message.prop('contenteditable', true);
        message.data('content', message.text());
        // TODO: replace message ids You/Peer with user IDs
        message.data('id', count);
        message.blur(function(){
            this.localEdit(message);
        }.bind(this));
        keyevent = function(e) {
            this.checkEnterEdit(e, message);
        }.bind(this);
        //TODO: separate into two functions (one for enter, one for esc)
        message.keypress(keyevent);
        message.keyup(keyevent);
    } else {
        message.data('activeEdits', 0); // number of edit text effects currently occuring in this message
        message.data('lastEditIndex', -1);
        message.data('editHistory', [message.text()]); // what this messages has been edited to most recently by remote
    }
    message.linkify();
    var mentionedVideos = $('<div/>', {
        'class': 'messenger-item'
    });
    var messenger = this;
    message.find('a.linkified').each(function(index) {
        var youtubeID = youtubeIDFromURL(this.href);
        if (youtubeID) {
            var loadTitle = $('<span/>', {
                'class': 'messenger-message messenger-youtube-title message-from-' + user
            });
            var img = $('<img/>', {
                'src': 'img/peer_typing_alpha22.gif'
            });
            var watchButton = $('<button/>', {
                type: 'button',
                'class': 'btn messenger-youtube-watch',
                text: 'Watch',
            }).click(function(ev) {
                this.$.find('.suggested-local.suggested-'+youtubeID).addClass('checked');
                // TODO change to cancel
                this.connection.suggestYoutubeToRemote(youtubeID);
                maybeStartWatching(this.id, youtubeID);
            }.bind(messenger));
            var suggestedWrapper = $('<span/>', {
                'class': 'suggested-wrapper suggested-wrapper-'+youtubeID,
            });
            var remoteSuggested = $('<input/>', {
                type: 'image',
                'class': 'suggested suggested-remote suggested-'+youtubeID,
                // TODO  remoteSuggested image
                src: 'img/male.png',
                name: youtubeID,
                value: 'remote',
            });
            var localSuggested = $('<input/>', {
                type: 'image',
                'class': 'suggested suggested-local suggested-'+youtubeID,
                // TODO localSuggested image
                src: 'img/male.png',
                name: youtubeID,
                value: 'local',
            });
            suggestedWrapper.append(localSuggested);
            suggestedWrapper.append(remoteSuggested);
            img.appendTo(loadTitle);
            loadTitle.appendTo(mentionedVideos);
            watchButton.appendTo(mentionedVideos);
            suggestedWrapper.appendTo(mentionedVideos);
            replaceWithYoutubeTitleFromID(youtubeID, img);
        }
    });
    this.putMessage(item);
    if (mentionedVideos.find('.messenger-message').length) {
        this.putMessage(mentionedVideos);
    }
    this.$.find('label#file-input-label').show();
}

function localFile(url, name, type) {
    if (isImageFile(name)) {
        return localImage(url);
    } else {
        // TODO make link to downlod from
        return localLink(url, name, type);
    }
}

Messenger.prototype.fileFrom = function (url, id, name) {
    if (isImageFile(name)) {
        return this.imageFrom(url, id, name);
    } else {
        return this.linkFrom(url, id, name);
    }
}

function localImage(url) {
    var item = document.createElement('div');
    item.classList.add("messenger-item");
    var img = document.createElement('img');
    img.id = url;
    img.classList.add("messenger-img");
    img.src = url;
    img.classList.add('message-from-You');
    item.appendChild(img);
    return item;
}

function localLink(url, name, type) {
    // FIXME on firefox this link doesn't download
    var item = document.createElement('div');
    item.classList.add("messenger-item");
    var span = document.createElement('span');
    span.classList.add('messenger-span');
    span.classList.add('message-from-You');

    var imageSpan = document.createElement('a');
    imageSpan.classList.add('glyphicon');
    imageSpan.classList.add('glyphicon-floppy-save');
    imageSpan.classList.add('messenger-download');
    imageSpan.classList.add('messenger-download-local');
    imageSpan.href = url;
    var a = document.createElement('a');
    a.classList.add('messenger-a');
    a.href = url;
    a.type = type;
    var filename = original_filename(name);
    a.innertText = a.textContent = filename;
    a.download = filename;
    imageSpan.type = type;
    imageSpan.download = filename;
    span.appendChild(imageSpan);
    span.appendChild(a);
    item.appendChild(span);
    return item;
}

/*
 * Replaces a placeholder using remote id
 */
Messenger.prototype.imageFrom = function(url, id, name) {
    var item = document.getElementById(fileMessageId(id, name));
    var toRemove = document.getElementById(filePlaceholderId(id, name));
    item.removeChild(toRemove);
    var img = document.createElement('img');
    img.classList.add("messenger-img");
    img.src = url;
    img.classList.add('message-from-Peer');
    item.appendChild(img);
}
/*
 * Replaces a placeholder using remote id
 */
Messenger.prototype.linkFrom = function (url, id, name) {
    var item = document.getElementById(fileMessageId(id, name));
    var toRemove = document.getElementById(filePlaceholderId(id, name));
    item.removeChild(toRemove);
    var span = document.createElement('span');
    span.classList.add('messenger-span');
    span.classList.add('message-from-Peer');

    var imageSpan = document.createElement('a');
    imageSpan.classList.add('glyphicon');
    imageSpan.classList.add('glyphicon-floppy-save');
    imageSpan.classList.add('messenger-download');
    imageSpan.classList.add('messenger-download-remote');
    imageSpan.href = url;
    var a = document.createElement('a');
    a.classList.add("messenger-a");
    // TODO a.type
    a.href = url;
    var filename = original_filename(name);
    a.innerText = a.textContent = filename;
    a.download = filename;
    imageSpan.download = filename;
    span.appendChild(imageSpan);
    span.appendChild(a);
    item.appendChild(span);
}

var TYPE_COUNT_SEND = 4;
var typeCount = TYPE_COUNT_SEND;
Messenger.prototype.putMessage = function(html) {
    // delete all messages about peers typing
    this.$.find('div.messenger-typing').remove();
    // reset typeCount for notification about your typing
    typeCount = TYPE_COUNT_SEND;

    this.$.find('.messages').append(html);
    this.$.find('.messages').scrollTop(this.$.find('.messages')[0].scrollHeight);
}
Messenger.prototype.previousMessage = function (user) {
    return this.$.find('span.message-from-' + user).last();
}

/* Returns the word to change, or null if no good word is found */
Messenger.prototype.maybeSubstitute = function (user, substitute) {
    // FIXME consider punctuation
    if (substitute.split(" ").length !== 1) {
        return false;
    }
    var message = this.previousMessage(user);
    var text = message.text();
    var words = wordsFrom(text);
    var ret = false;
    var minDist = words.map(function(w) {
        return [w, levenshteinDistance(w, substitute)];
    }).reduce(function(a, b) {
        return b[1] < a[1] ? b : a;
    });
    if (minDist[1] < 3)
        return minDist[0];
    return null;
}

/*
 * When send button is pressed
 */
Messenger.prototype.localSend = function localSend() {
    var textInput = this.$.find('.messenger-text-input'); 
    var message = textInput.val();
    if (message.trim() == '') {
        // do not send empty messages
        return;
    }
    textInput.val('');
    if (message.charAt(0) == '*') {
        var substitution = message.substring(1);
        var subWord = this.maybeSubstitute('You', substitution);
        if (subWord && subWord != substitution) {
            var message = this.previousMessage('You');
            var newHtml = replaceLastInstance(message.html(), subWord, substitution);
            message.html(newHtml);
            this.connection.editToRemote(message.html(), message.data('id'));
            return;
        }
    }
    this.putMessageFrom("You", message);
    // putMessage(messageFrom("You", message));
    this.connection.sendToRemote(message);
}

//http://stackoverflow.com/questions/2729666/javascript-replace-last-occurence-of-text-in-a-string
function replaceLastInstance(str, oldStr, newStr) {
    var charpos = str.lastIndexOf(oldStr);
    if (charpos<0) return str;
    ptone = str.substring(0,charpos);
    pttwo = str.substring(charpos+(oldStr.length));
    return (ptone+newStr+pttwo);
}

/**
 * We have a received a file from remote and we need to make
 * a placeholder to put it.
 * id is the id of the user
 * name is the name of the file
 */
Messenger.prototype.remotePlaceholder = function (id, name, fileNote) {
    if (fileNote.height) {
        var html = imagePlaceholder("Peer", id, name, fileNote);
    } else {
        var html = filePlaceholder("Peer", id, name);
    }
    this.putMessage(html);
}

Messenger.prototype.localFiles = function (ev) {
    var fileInput = this.$.find('.messenger-file-input')[0];
    var files = fileInput.files;
    for (var index = 0; index < files.length; index++) {
        var file = files[index];
        makeLocalCopy(file, this, this.connection);
    }
    // FIXME wrong value of this here now
    //clears fileInput so onchange fires for Chrome
    fileInput.value = null;
}

Messenger.prototype.localEdit = function(message) {
    if (message.data('content') != message.text()) {
        message.unlinkify();
        this.connection.editToRemote(message.html(), message.data('id'));
        message.data('content', message.text());
        message.linkify();
    }
}

/**
 * When a message comes from remote
 */
Messenger.prototype.remoteSend = function(message) {
    if (!this.$.find('.messenger-text-input').is(':focus')) {
        // only notify if not active tab
        if (!this.tab.hasClass('chattab-selected')) {
            tabNotify(this.tab);
        }
        if (document.hidden) {
            useNotificationTitle();
        }
    }
    this.putMessageFrom("Peer", message);
}

/**
 * When a message edit comes from remote
 */
Messenger.prototype.remoteEdit = function(edit, message_id) {
    if (!this.tab.hasClass('chattab-selected')) {
        tabNotify(this.tab);
    }
    var message = this.$.find('#message-Peer-' + message_id);
    message.data('editHistory').push(edit);
    this.$.find('div.messenger-typing').remove();
    startEditAnimation(message);
}

function startEditAnimation(message) {
    if (message.data('lastEditIndex') < message.data('editHistory').length - 1
      && message.data('activeEdits') == 0) {
        message.data('lastEditIndex', message.data('editHistory').length - 1);
        edits = message.data('editHistory');
        edit = edits[edits.length - 1];
        message.unlinkify();
        message.html(wordDiff(message.text(), edit).replace('\n', '<br/>'));
        message.linkify();
        message.find('.messenger-del').each(startDelete);
        message.find('.messenger-ins').each(startInsert);
    }
}

function startDelete(i, del) {
    del = $(del)
    var message = del.parent();
    editStart(message);
    del.textEffect({
        effect: 'shrink',
        onFinish: function(elem) {
            elem.remove();
            editDone(message);
        }
    });
}

function startInsert(i, ins) {
    ins = $(ins)
    var message = ins.parent();
    editStart(message);
    ins.textEffect({
        effect: 'grow',
        onFinish: function(elem) {
            elem.contents().unwrap();
            editDone(message);
        }
    });
}

function editStart(message) {
    count = message.data('activeEdits') + 1;
    message.data('activeEdits', count);
}

function editDone(message) {
    count = message.data('activeEdits') - 1;
    message.data('activeEdits', count);
    if (count == 0) {
        startEditAnimation(message);
    }
}

// applies animation effects to insertions and deletions
function applyEffects() {
    $('.messenger-del').textEffect({
        effect: 'shrink',
        completionSpeed: 2000,
        onFinish: function(elem) {elem.remove();}
    });
    $('.messenger-ins').textEffect({
        effect: 'grow',
        completionSpeed: 2000,
        onFinish: function(elem) {elem.contents().unwrap();}
    });
}

/**
 * When remote is typing a message
 */
Messenger.prototype.remoteTyping = function() {
    if (this.$.find('div.messenger-typing').length == 0) {
        this.$.find('.messages').append(
            "<div class='messenger-typing'>" + 
              "<span class='messenger-message message-from-Peer'>" +
                "<img src='img/peer_typing_alpha22.gif'/>" +
              "</span>" +
            "</div>"
        );
    }
}

/**
 * When remote is not doing anything
 */
Messenger.prototype.remoteIdle = function() {
    this.$.find('div.messenger-typing').remove();
}

// TODO object these
var idleTime = 0;
var idleTimer;
Messenger.prototype.onTimeInterval = function() {
    idleTime++;
    if (idleTime == 3) {
        this.connection.idleToRemote();
    }
}
/*
 * If they pressed enter then they pressed the button
 * Also tell the peer maybe
 * These messages are removed in putMessage
 */
Messenger.prototype.onMessengerTyped = function (e) {
    var code = e.keyCode ? e.keyCode : e.which;
    // check for non-shift ENTER
    if (code == KEYCODE_ENTER && !e.shiftKey) {
      // capture that keypress and send message instead
      this.localSend();
      return false;
    }
    if (typeCount > TYPE_COUNT_SEND) {
        this.connection.typingToRemote();
        typeCount = 0;
        idleTime = 0;
    } else {
        typeCount++;
    }
    return true;
}

/*
 * If they pressed enter while editing a message, they remove focus
 */
Messenger.prototype.checkEnterEdit = function(e, message) {
    code = e.keyCode ? e.keyCode : e.which;
    if (code == KEYCODE_ENTER && !e.shiftKey) {
        // capture that keypress and send edited message instead
        message.blur();
        return false;
    } else {
        if (code == KEYCODE_ESC) {
            message.text(message.data('content'));
            message.blur();
        }
    }
    return true;
}

/**
 * We care about Chrome, Firefox, and Opera right now because they have WebRTC
 * http://en.wikipedia.org/wiki/Comparison_of_web_browsers#Image_format_support
*/
function isImageFile(filename) {
    extension = filename.split('.').pop().toLowerCase();
    var image_extensions = [
        'jpeg',
        'jpg',
        'gif',
        'png'
    ];
    return image_extensions.indexOf(extension) > -1;
}

// FIXME does this have to run before page load?
// FIXME does this actually have to run on keyup?
$('textarea.messenger-text-input').on('keyup change paste', function() {
  $('textarea.messenger-text-input').css('height', '0px');
  $('textarea.messenger-text-input').css(
    'height',
    $('textarea.messenger-text-input')[0].scrollHeight+3+'px')
  if ($(this).val() == '') {
    $('label.file-input-label').show();
  } else {
    $('label.file-input-label').hide();
  }
});

function wordsFrom(str) {
    return str.split(/[^\w\']+/);
}

function levenshteinDistance(a, b) {
    // http://en.wikibooks.org/wiki/Algorithm_Implementation/Strings/Levenshtein_distance#JavaScript
   if(a.length === 0) return b.length; 
   if(b.length === 0) return a.length; 
      
   var matrix = [];
         
   // increment along the first column of each row
   var i;
   for(i = 0; i <= b.length; i++){
     matrix[i] = [i];
   }
                      
   // increment each column in the first row
   var j;
   for(j = 0; j <= a.length; j++){
     matrix[0][j] = j;
   }
          
   // Fill in the rest of the matrix
   for(i = 1; i <= b.length; i++){
     for(j = 1; j <= a.length; j++){
       if(b.charAt(i-1) == a.charAt(j-1)){
         matrix[i][j] = matrix[i-1][j-1];
       } else {
         matrix[i][j] = Math.min(matrix[i-1][j-1] + 1, // substitution
         Math.min(matrix[i][j-1] + 1, // insertion
         matrix[i-1][j] + 1)); // deletion
         if (i > 1 && j > 1 && a.charAt(i-2) == b.charAt(j-1) &&
             a.charAt(i-1) == b.charAt(j-2)) { // swap 
           matrix[i][j] = Math.min(matrix[i][j], matrix[i-2][j-2] + 1);
         }
       }
     }
   }
   return matrix[b.length][a.length];
}

// call this once
onResize();
