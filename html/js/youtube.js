function makeVideo(messengerID, youtubeID) {
    console.log('makeVideo');
    var messenger = messengers[messengerID];
    var videoID = messenger.getYoutubeVideoID();
    // replaces the element with an iframe with same id, class
    // https://developers.google.com/youtube/player_parameters
    var player = new YT.Player(videoID, {
        height: '390',
        width: '640',
        videoId: youtubeID,
        events: {
            onReady: onPlayerReady,
            onStateChange: onPlayerStateChange,
        },
        playerVars: {
            // TODO(iso) h1: 'en',
            'controls': 0, // no controls
            'iv_load_policy': 3, // no annotations
            'showinfo': 0, // no video title
            'rel': 0, // no related videos at end
            'modestbranding': 0, // sorry Google
        },
    });
    player.ignoreStateChange = {};
    player.messenger = messenger;
    messenger.player = player;
    $('#'+videoID).show();
    messenger.$.find('.messenger-youtube-ex').show();
    // TODO adjust top offset for youtube
}
function hideAndForceBuffer(player) {
    console.log('hideAndForceBuffer');
    var messenger = player.messenger;
    var youtube = player.getIframe();
    player.mute();
    player.onMessengerPlayerStateChange = onLoadingStateChange;
    player.playVideo();
}

function onPlayerStateChange(event) {
    console.log('onPlayerStateChange ' + event.data + ' ' + new Date().getTime());
    var player = event.target;
    var state = event.data;
    if (player.ignoreStateChange[state]) {
        console.log('ignoring state change');
        player.ignoreStateChange[state] = undefined;
        return;
    }
    if (player.onMessengerPlayerStateChange) {
        player.onMessengerPlayerStateChange(event);
    }
}

/**
 *  triggers on play, then on pause, then we are ready
 */
function onLoadingStateChange(event) {
    console.log('onLoadingStateChange');
    var player = event.target;
    var messenger = player.messenger;
    var state = event.data;
    var youtube = player.getIframe();
    switch(state) {
        case YT.PlayerState.PLAYING:
            player.pauseVideo();
            player.seekTo(0);
            break;
        case YT.PlayerState.PAUSED:
            player.onMessengerPlayerStateChange = undefined;
            var videoID = player.getVideoData().video_id;
            messenger.connection.readyYoutubeToRemote(videoID);
            player.localReady = true;
            maybePlayVideo(player);
            break;
    }
}

function onRemoteReady(player) {
    console.log('onRemoteReady');
    player.remoteReady = true;
    maybePlayVideo(player);
}

function onRemotePause(player, time) {
    console.log('onRemotePause');
    if (player.getPlayerState() === YT.PlayerState.PAUSED) {
        return;
    }
    player.ignoreStateChange[YT.PlayerState.PAUSED] = true;
    player.pauseVideo();
    player.seekTo(time, true);
}

function onRemotePlay(player) {
    console.log('onRemotePlay');
    if (player.getPlayerState() === YT.PlayerState.PLAYING) {
        return;
    }
    player.ignoreStateChange[YT.PlayerState.PLAYING] = true;
    player.playVideo();
}

function maybePlayVideo(player) {
    console.log('maybePlayVideo');
    if (player.remoteReady && player.localReady) {
        player.onMessengerPlayerStateChange = onPlayingStateChange;
        // autoplay the video
        player.ignoreStateChange[YT.PlayerState.PLAYING] = true;
        player.playVideo();
        player.unMute();
    }
}

function makeOnYoutubeEx(messengerID) {
    return function onYoutubeEx(event) {
        console.log(event);
        destroyVideo(messengerID)
        $(this).hide();
    }
}

function destroyVideo(messengerID) {
    console.log('destroyVideo');
    var player = messengers[messengerID].player;
    var messenger = player.messenger;
    messenger.connection.closeYoutubeToRemote();
    player.stopVideo();
    player.clearVideo();
    // destroy() also removes the iframe
    player.destroy();
    messenger.constructYoutubeVideo(
    ).hide(
    ).after(messenger.$.find('.messenger-local-video')
    );
    player.messenger = messenger.player = undefined;
}
function onPlayerReady(event) {
    console.log('onPlayerReady');
    console.log(event);
    var player = event.target;
    hideAndForceBuffer(player);
}
function onPlayingStateChange(event) {
    console.log('onPlayingStateChange');
    var player  = event.target;
    var messenger = player.messenger;

    // TODO reorganize ignoreStateChange as a Counter instead of boolean dictionary
    // TODO handle pause->buffer->pause and play->buffer->play
    switch (event.data) {
        case YT.PlayerState.PLAYING:
            messenger.connection.playToRemote();
            break;
        case YT.PlayerState.PAUSED:
            messenger.connection.pauseToRemote(player.getCurrentTime());
            break;
    }
}
function youtubeIDFromURL(url) {
    // http://stackoverflow.com/questions/3452546/javascript-regex-how-to-get-youtube-video-id-from-url
    var regExp = /^.*((youtu.be\/)|(v\/)|(\/u\/\w\/)|(embed\/)|(watch\?))\??v?=?([^#\&\?]*).*/;
    var match = url.match(regExp);
    if (match && match[7].length==11){
        return match[7];
    } else {
        return undefined;
    }
}
function replaceWithYoutubeTitleFromID(youtubeID, putItHere) {
    // FIXME find the Youtube v3 way to do this
    $.get('http://gdata.youtube.com/feeds/api/videos/'+youtubeID+'?v=2&fields=title', function ( data ) {
        title = $(data).find('title').text();
        putItHere.replaceWith(title);
    });
}
function maybeStartWatching(messengerID, youtubeID) {
    console.log('maybeStartWatching');
    if ($('.suggested-local.checked.suggested-'+youtubeID).length > 0
    && $('.suggested-remote.checked.suggested-'+youtubeID).length > 0) {
        makeVideo(messengerID, youtubeID);
    }
}
