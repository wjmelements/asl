<?php

require_once('constants.php');
require_once('enums.php');
require_once('languages.php');

// packs mostly the same, but with password length instead of ASL
function pack_friend_request($password_len, $id, $message_type, $json_len) {
    return pack_match_request($password_len, '', '', '', $id, $message_type, $json_len);
}

function pack_match_request(
    $age,
    $sex,
    $looking,
    $languages,
    $id,
    $message_type,
    $json_len
) {
    $LANGUAGE_ENUM = $GLOBALS['LANGUAGE_ENUM'];
    $language_bitmasks = array(0, 0, 0, 0, 0, 0);
    for ($i = 0; $i < strlen($languages); $i += 2) {
        $stats = $LANGUAGE_ENUM[substr($languages, $i, 2)];
        $language_bitmasks[$stats['V_num']] |= $stats['val'];
    }
    // see include/request.h
    return pack(
        'vx2v4V8v',
        intval($message_type), // vx2
        intval($age), // v
        intval($sex == "m"), // v
        intval($looking == "m" || $looking == "mf"), // v
        intval($looking == "f" || $looking == "mf"), // v
        // The next six lines are all languages
        $language_bitmasks[0], // V
        $language_bitmasks[1], // V
        $language_bitmasks[2], // V
        $language_bitmasks[3], // V
        $language_bitmasks[4], // V
        $language_bitmasks[5], // V
        $id & 0xFFFFFFFF, // V
        $id >> 32, // V
        $json_len // v
    );
}

function unpack_ps_reply($buffer, &$json_len) {
    // see include/reply.h
    $assoc = unpack("vsuccess/vpadding/V2id/vage/vmale/vjson_len", $buffer);
    if (!$assoc) {
        // this means that this file is out of sync with include/reply.h
        return array('message' => "no assoc");
    }
    if ($assoc['success']) {
        $idl = ($assoc['id2'] << 32) | $assoc['id1'];
        $age = $assoc['age'];
        $male = $assoc['male'];
        $json_len = $assoc['json_len'];
        return array('id' => $idl, 'age' => $age, 'male' => $male);
    } else {
        // pair server did not succeed
        switch ($assoc['id1']) {
            case BAD_REQUEST:
                return array('message' => "bad request");
            case SERVER_SHUTDOWN:
                return array('message' => "server shutdown");
            case REQUEST_TIMEOUT:
                return array('message' => "request timeout");
            case CANCELED:
                return array('message' => "canceled", 'canceled' => true);
            case NOT_CANCELED:
                return array('message' => "not canceled", 'canceled' => false);
            case ALREADY_REQUEST:
                return array('message' => "already requested");
        }
    }
}

function connect_to_ps($params, $id, $message_type, $json_obj) {
    // TODO AF_UNIX
    $socket = socket_create(AF_INET, SOCK_STREAM, SOL_TCP);
    if (!is_resource($socket)) {
        exit(json_encode(array('message' => "Failed to create socket")));
    }
    if (!socket_set_option($socket, SOL_SOCKET, SO_RCVTIMEO, array("sec"=> 0, "usec"=> 0))) {
        exit(json_encode(array('message' => "Failed to set socket options")));
    }
    $address = "127.0.0.1";
    if (__FILE__ != '/var/www/html/structs.php') {
        // is dev user
        $third_slash = strpos(__FILE__, '/', 7);
        $second_slash = 6; // '/home/'
        $user = substr(__FILE__, $second_slash, $third_slash - $second_slash);
        $port = intval(posix_getpwnam($user)['uid']) + 30000;
    } else {
        $port = 2423;
    }
    if (!socket_connect($socket, $address, $port)) {
        exit(json_encode(array('message' => "Failed to connect to pair server")));
    }
    $idl = intval($id);
    if (action($message_type) == MESSAGE_TYPE_CANCEL) {
        // no need to pass object for cancellation
        $json_len = 0;
    } else {
        $json_len = strlen($json_obj);
    }
    $is_match = meet($message_type) === MEET_TYPE_MATCH;
    if ($is_match) {
        $buf = pack_match_request(intval($params['age']), $params['sex'], $params['looking'], $params['language'], $idl, $message_type, $json_len);
    } else {
        $buf = pack_friend_request(strlen($params['password']), $idl, $message_type, $json_len);
    }
    $len = REQUEST_SIZE;
    $flags = 0;
    if (socket_send($socket, $buf, $len, $flags) !== $len) {
        exit(json_encode(array('message' => "Failed to send the data")));
    }
    if (!$is_match && action($message_type) == MESSAGE_TYPE_SEND) {
        $password = $params['password'];
        $len = strlen($password);
        if (!socket_send($socket, $password, $len, $flags) == $len) {
            exit(json_encode(array('message' => "Failed to send the password")));
        }
    }
    if ($json_len) {
        if (!socket_send($socket, $json_obj, $json_len, $flags) == $json_len) {
            exit(json_encode(array('message' => "Failed to send the object")));
        }
    }
    $len = REPLY_SIZE;
    if (!socket_recv($socket, $buf, $len, MSG_WAITALL) == $len) {
        exit(json_encode(array('message' => "Failed to receive the data")));
    }
    $reply = unpack_ps_reply($buf, $json_len);
    if (@$reply['message']) {
        // error in unpack_ps_reply
        exit(json_encode($reply));
    }
    if ($json_len) {
        if (!socket_recv($socket, $buf, $json_len, MSG_WAITALL) == $json_len) {
            exit(json_encode(array('message' => "Failed to receive the object")));
        }
    }
    socket_close($socket);
    $reply['obj'] = json_decode($buf);
    exit(json_encode($reply));
}

function verify_description($obj) {
    if (@$obj['offer'] && @$obj['answer']) {
        exit(json_encode(array('message' => 'invalid session description')));
    }
    $description = @$obj['offer'] ?:  $obj['answer'];
    if (!$description) {
        exit(json_encode(array('message' => 'invalid session description')));
    }
    $sdp = $description['sdp'];
    if (!$sdp) {
        exit(json_encode(array('message' => 'invalid session description')));
    }
    if (strpos($sdp, 'webrtc-datachannel') == -1) {
        exit(json_encode(array('message' => 'invalid session description')));
    }
}

function verify_candidates($obj) {
    $session = @$obj['offer'] || $obj['answer'];
    if (count($obj['candidates']) == 0) {
        exit(json_encode(array('message' => 'invalid ICE candidates')));
    }
    $contains_relay = false;
    foreach ($obj['candidates'] as $candidate) {
        // TODO more individual candidate validation
        // http://www.ietf.org/rfc/rfc5245.txt
        $cand_str = $candidate['candidate'];
        $typ_start = strpos($cand_str, "typ") + 4;
        if ($typ_start == -1) {
            exit(json_encode(array('message' => 'invalid ICE candidates')));
        }
        $typ_end = strpos($cand_str, ' ', $typ_start);
        if ($typ_end == -1) {
            exit(json_encode(array('message' => 'invalid ICE candidates')));
            exit;
        }
        $typ = substr($cand_str, $typ_start, $typ_end - $typ_start);
        if ($typ === "relay") {
            $contains_relay = true;
        }
    }
    if (!$contains_relay) {
        exit(json_encode(array('message' => 'invalid ICE candidates')));
    }
}

function verify_obj($obj) {
    verify_description($obj);
    verify_candidates($obj);
}

function verify_match_input($age, $sex, $looking, $language) {
    if (!(ctype_digit($age)
        && intval($age) <= 120
        && intval($age) >= 14
    )) {
        exit(json_encode(array('message' => "invalid age: $age")));
    }
    if (!($sex == "m" || $sex == "f")) {
        exit(json_encode(array('message' => "invalid sex: $sex")));
    }
    if (!($looking == "m" || $looking == "f" || $looking == "mf")) {
        exit(json_encode(array('message' => "invalid looking: $looking")));
    }
    $language_size = strlen($language);
    if ($language_size == 0 || $language_size % 2 == 1) {
        exit(json_encode(array('message' => "invalid language length: $language")));
    }
    $LANGUAGES = $GLOBALS['languages'];
    for ($i = 0; $i < $language_size; $i += 2) {
       if (!array_key_exists(substr($language, $i, 2), $LANGUAGES)) {
           exit(json_encode(array('message' => "invalid language: $language")));
       }
    }
}

function verify_friend_input($password) {
    if (strlen($password) > 256) {
        exit(json_encode(array('message' => 'password too long')));
    }
}
function verify_id($id) {
    if (!is_numeric($id)) {
        exit(json_encode(array("invalid id: $id")));
    }
    // TODO verify that id is a signed integer
}

function process_parameters($action_type) {
    // read raw POST vars
    $is_match = isset($_POST['age']);
    if ($is_match) {
        $message_type = request_type_from($action_type, MEET_TYPE_MATCH);
        $age = @$_POST['age'];
        $sex = @$_POST['sex'];
        $looking = @$_POST['looking'];
        $language = @$_POST['language'];
        verify_match_input($age, $sex, $looking, $language);
    } else {
        $message_type = request_type_from($action_type, MEET_TYPE_FRIEND);
        $password = @$_POST['password'];
        verify_friend_input($password);
    }
    $id = @$_POST['id'];
    verify_id($id);
    $obj = @$_POST['obj'];
    if (action($message_type) != MESSAGE_TYPE_CANCEL) {
        verify_obj($obj);
    }
    $json_obj = json_encode($obj);
    // success
    connect_to_ps($_POST, $id, $message_type, $json_obj);
}

if (@$_SERVER['REQUEST_METHOD'] !== 'POST' || !$_POST) {
    // it was not a valid post request, so redirect to main page
    header('Location:index.php');
    return;
}
