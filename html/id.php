<?php

function gen_ip_info() {
    $ip = $_SERVER['REMOTE_ADDR'];
    return json_decode(file_get_contents("http://ipinfo.io/{$ip}"));
}
function gen_turn_servers($id) {
    // TODO get our own turn servers
    return json_decode(file_get_contents("http://computeengineondemand.appspot.com/turn?username=$id"));
}

function gen_id_and_turn() {
    $id = gen_id();
    $turn = gen_turn_servers($id);
    $ret = array(
        'id' => $id,
        'turn' => $turn,
    );
    return $ret;
}

function unpack_id_reply($buffer) {
    // see include/reply.h
    $assoc = unpack("V2id", $buffer);
    if (!$assoc || !$assoc['id1'] && !$assoc['id2']) {
        exit(json_encode(array('message'=>"no assoc")));
    }
    $id = ($assoc['id1'] << 32) | $assoc['id2'];
    return $id;
}

function gen_id() {
    // TODO AF_UNIX
    $socket = socket_create(AF_INET, SOCK_STREAM, SOL_TCP);
    if (!$socket) {
        echo json_encode(array('message'=>"Failed to create socket"));
        exit;
    }
    $address = "127.0.0.1";
    if (__FILE__ != '/var/www/html/id.php') {
        // is dev user
        $third_slash = strpos(__FILE__, '/', 7);
        $second_slash = 6; // '/home/'
        $user = substr(__FILE__, $second_slash, $third_slash - $second_slash);
        $port = intval(posix_getpwnam($user)['uid']) + 20000;
    } else {
        $port = 2422;
    }
    if (!socket_connect($socket, $address, $port)) {
        exit(json_encode(array('message'=>"Failed to connect to id server")));
    }
    $len = 8;
    $buf = "";
    if (socket_recv($socket, $buf, $len, MSG_WAITALL) !== $len) {
        exit(json_encode(array('message'=>"Failed to receive the data")));
    }
    $id = unpack_id_reply($buf);
    return $id;
}

function gen_response() {
    $id_and_turn = gen_id_and_turn();
    $ipcontents = gen_ip_info();
    $region = $ipcontents ? $ipcontents->region : 'Unknown';
    $country = $ipcontents ? $ipcontents->country : 'Unknown';
    $id = $id_and_turn['id'];
    $turn = $id_and_turn['turn'];
    exit(json_encode(array(
        'id' => $id,
        'region' => $region,
        'country' => $country,
        'turn' => $turn,
    )));
}

// form.js should post with no data
if ($_SERVER['REQUEST_METHOD'] === 'POST' && !$_POST) {
    gen_response();
} else {
    // it was not a valid request, so redirect to main page
    header('Location:index.php');
    exit;
}
