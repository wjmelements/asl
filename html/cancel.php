<?php
require_once('structs.php');
require_once('constants.php');

/*
 * Verifies the parameters of the request, and removes it from the queues in the
 * pair server.
 */
process_parameters(MESSAGE_TYPE_CANCEL);
?>
