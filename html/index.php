<?php
  require_once('languages.php');
  $form_type = isset($_GET['password']) && !isset($_GET['age']) ? 'password' : 'asl';
  $age = @$_GET['age'] ?: 18;
  if (is_numeric($age))
    $age = floor($age);
  else
    $age = 18;
  $sex = @$_GET['sex'] ?: '';
  $looking = @$_GET['looking'] ?: '';
  $sex_male_checked = $sex == 'm' ? 'checked' : '';
  $sex_female_checked = $sex == 'f' ? 'checked' : '';
  $looking_male_checked = strpos($looking, 'm') !== false ? 'checked' : '';
  $looking_female_checked = strpos($looking, 'f') !== false ? 'checked' : '';
  $password = @$_GET['password'] ?: '';
  $url_language = @$_GET['language'] ?: '';
  $preferred_languages = array();
  global $languages;
  if ($url_language === '') {
    // Assuming that languages are in order of preference
    if (array_key_exists("HTTP_ACCEPT_LANGUAGE", $_SERVER)) {
        $locale = $_SERVER["HTTP_ACCEPT_LANGUAGE"];
        $locale_array = explode(',', $locale);
        foreach ($locale_array as $language) {
          $iso = substr($language, 0, 2);
          if ($iso && array_key_exists($iso, $languages)
           && !in_array($iso, $preferred_languages)) {
            array_push($preferred_languages, $iso);
          }
        }
    }
  } else {
    $num_languages = strlen($url_language) / 2;
    for ($i = 0; $i < $num_languages; $i++) {
      $iso = substr($url_language, $i * 2, 2);
      if (array_key_exists($iso, $languages)
       && !in_array($iso, $preferred_languages)) {
        array_push($preferred_languages, $iso);
      }
    }
  }
?>

<!DOCTYPE html>
<!-- saved from url=(0039)http://getbootstrap.com/examples/cover/ -->
<html>
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link href='http://fonts.googleapis.com/css?family=Nunito:300' rel='stylesheet' type='text/css'>
    <title id="page-title">meerchat</title>

    <!-- Bootstrap core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet"> 
    <!-- Custom styles for this template -->
<?php // only use compressed css in production
if ($_SERVER['PHP_SELF'] === '/index.php') {?>
    <link href="css/min/cover.min.css" rel="stylesheet">
<?php } else { ?>
    <link href="css/cover.css" rel="stylesheet">
<?php } ?>
    <link href="favicon.ico" rel="icon" >

    <style type="text/css"></style>
    <style id="holderjs-style" type="text/css"></style>
  </head>

  <body style="background-image: url('img/image<?php
      echo rand(3,5);
  ?>_blur.jpeg');">
    <script>
      window.languages =
      <?php
      // TODO prefetch languages for typeahead instead
      global $languages;
      echo json_encode($languages);
      ?>;
      window.probableLanguages =
      <?php
      echo json_encode($preferred_languages);
      ?>;
      window.form = '<?php echo $form_type; ?>';
    </script>
    <div class="site-wrapper">
      <div class="site-wrapper-inner">
        <div class="cover-container">
          <h1 class="cover-heading">meerchat</h1>
          <div id='btn-chattab-wrapper' class='btn-group hidden'>
           <div id="btn-chattab-plus-wrapper" class="btn-group">
              <a id="btn-chattab-plus" class="btn-chattab">
                <span class="glyphicon glyphicon-plus-sign roll-plus-button plus-button"></span>
              </a>
            </div> 
          </div> <!-- End Button Group -->
          <div class="inner cover" id="Form">
            <div id="main_card">
              <div id="banner">
              </div>
              <div id="main-card-tabs">
                <a href="#" id="asl-form-tab" class="selected-tab" onclick="aslForm()">random</a>
                <a href="#" id="password-form-tab" onclick="passwordForm()">friend</a>
              </div> <!-- tabs -->
              <div id="main_table" class="table-wrapper"> 
                <table class="table no-border" id="asl-form"> 
                  <tr class="asl-row">
                    <td class="aslLabel">
                      <div id="age-text">age</div>
                    </td>
                    <td colspan="2">
                      <input type="number" class="form-control" id="input_age" value="<?php echo $age ?>" min="14" max="120">
                    </td>
                  </tr>

                  <tr class="asl-row" >
                    <td class="aslLabel">
                      <div id="sex-text">sex</div>
                    </td>
                    <td>
                      <div class="gender-button-wrapper">
                        <div class="sex-button">
                          <input type="image" name="sex" id="input_male" class="male sex <?php echo $sex_male_checked ?>" value="Male" src="img/male.png">
                        </div>
                        <div class="sex-button">
                          <input type="image" name="sex" id="input_female" class="female sex <?php echo $sex_female_checked ?>" value="Female" src="img/female.png">
                        </div>
                      </div>
                    </td>
                  </tr>

                  <tr class="asl-row">
                    <td class="aslLabel">
                      <div id="looking-text">looking</div>
                    </td>
                    <td>
                      <div class="gender-button-wrapper">
                        <div class="looking-button">
                          <input type="image" name="looking" id="input_looking_male" class="male looking <?php echo $looking_male_checked ?>" value="Male" src="img/male.png">
                        </div>
                        <div class="looking-button">
                          <input type="image" name="looking" id="input_looking_female" class="female looking <?php echo $looking_female_checked ?>" value="Female" src="img/female.png">
                        </div>
                      </div>
                    </td>
                  </tr>
                  <tr class='asl-row'>
                    <td class='aslLabel'>
                      <div id="language-text">language</div>
                    </td>
                    <td colspan="2">
                      <button id="language-toggle" class="btn"><?php
                          $num_languages = count($preferred_languages);
                          if ($num_languages > 0) {
                            echo $languages[$preferred_languages[0]]["native"][0] . ($num_languages > 1 ? ' +' . (count($preferred_languages) - 1) : '');
                          } else {
                            echo "Select";
                          }
                        ?></button>
                    </td>
                  </tr>
                  <tr class="asl-row language-view" hidden>
                    <td colspan="3">
                    <div id="input_languages_wrapper" class="modal-body">
                      <div id="selected-languages" class="text">selected languages:</div>
                      <div id="input_languages_group"></div>
                      <input type="text" id="input_languages" placeholder="search for a language" class="typeahead form-control" autocomplete="off">
                    </div>
                    </td>
                  </tr>
                  <tr class='asl-row share-location-row'>
                    <td class='aslLabel'>
                      <div id="location-text">location</div>
                    </td>
                    <td colspan="2">
                      <div id="input_location_wrapper">
                        <button type="button" id="asl-location-button" class="btn input_button_on input_location">
                          <img class='location-placeholder' src="img/peer_typing_alpha22.gif">
                        </button>
                      </div>
                    </td>
                  </tr>
                </table>
                <table class="table no-border" id="password-form">
                  <tr class="password-row">
                    <td class="password-label">
                      <div id="password-text">password</div>
                      <div>
                        <input type="text" id="input-password" maxlength="255" value=<?php echo $password; ?>>
                        <button type="button" class="btn input_button_on pwgen-btn" onclick="genPassword()">
                          Generate
                        </button>
                      </div>
                    </td>
                  </tr>
                  <tr class='password-row share-location-row'>
                    <td colspan="3" class='aslLabel'>
                      <div id="location-text">location</div>
                      <div id="input_location_wrapper">
                        <button type="button" class="btn btn-sm input_button_on input_location">
                          <img class='location-placeholder' src="img/peer_typing_alpha22.gif">
                        </button>
                      </div>
                    </td>
                  </tr>
                </table>
              </div> <!-- Main table -->
              <script>
                <!-- SEARCH BUTTON -->
                document.write('<button type="button" class="btn" id="asl_search" onClick="verifyInput()" disabled><img class="search-placeholder" src="img/peer_typing_alpha22.gif"></button>');
              </script>
              <noscript>
                <div class="alert alert-danger" id="javascript_message">This site requires javascript.</div>
              </noscript>

            </div>
          </div> <!-- End Form -->
          <div class="row">
            <div id="input_message"></div>
          </div>
        </div>
        <div id="bottom-links">
          <a target="_blank" href="about.php">
            about
          </a>
          <a target="_blank" href="mailto:meerchat.feedback@gmail.com?subject=Meerchat">
            contact
          </a>
        </div>
      </div>
    </div>
    <!-- Place all scripts at the end of the document so the pages load faster -->
    <!-- JQuery -->
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
    <!-- ASL JavaScript -->
<?php // only use compressed javascript in production
if ($_SERVER['PHP_SELF'] === '/index.php') {?>
    <script src="js/min/form.min.js"></script>
    <script src="js/min/webrtc.min.js"></script>
    <script async src="js/min/video.min.js"></script>
    <script async src="js/min/messenger.min.js"></script>
    <script src="js/min/title.min.js"></script>
    <script async src="js/min/file.min.js"></script>
    <script async src="js/min/diff.min.js"></script>
    <script async src="js/min/youtube.min.js"></script>
<?php } else { ?>
    <script src="js/form.js"></script>
    <script src="js/webrtc.js"></script>
    <script async src="js/video.js"></script>
    <script async src="js/messenger.js"></script>
    <script src="js/title.js"></script>
    <script async src="js/file.js"></script>
    <script async src="js/diff.js"></script>
    <script async src="js/youtube.js"></script>
<?php } ?>
    <!-- Bootstrap core JavaScript -->
    <script src="js/bootstrap.min.js"></script>
    <script src="js/docs.min.js"></script>
    <!-- Twitter typeahead.js -->
    <script src="js/typeahead.jquery.min.js"></script>
    <!-- Google WebRTC Adapter -->
    <script src="js/adapter.js"></script>
    <!-- Linkify messages -->
    <script async src="js/jquery.linkify.js"></script>
    <!-- Text Effect plugin -->
    <script async src="js/min/textEffect.jquery.min.js"></script>
    <!-- Youtube API -->
    <script async src="https://www.youtube.com/iframe_api"></script>
  </body>
</html>
