<html>
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link href='http://fonts.googleapis.com/css?family=Nunito:300' rel='stylesheet' type='text/css'>
    <title id="page-title">meerchat</title>

    <!-- Bootstrap core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet"> 
    <!-- Custom styles for this template -->
<?php // only use compressed css in production
if ($_SERVER['PHP_SELF'] === '/index.php') {?>
    <link href="css/min/cover.min.css" rel="stylesheet">
<?php } else { ?>
    <link href="css/cover.css" rel="stylesheet">
<?php } ?>
    <link href="css/about.css" rel="stylesheet">

    <style type="text/css"></style>
    <style id="holderjs-style" type="text/css"></style>
  </head>

  <body style="background-image: url('img/image5_blur.jpeg');">
    <div class="site-wrapper">
      <div class="site-wrapper-inner">
        <div class="cover-container">
          <h1 class="cover-heading">meerchat</h1>
        </div>
        <div class="about-paragraph">
          <img src="img/meerkat.png" id="about-meerkat">
          <p>
            <a href="/">meerchat</a> is a peer-to-peer messaging service; your messages go from your browser to another and do not touch our server. When you close the tab, there will be no record of your messages except for your peer's browser. 
          </p>
          <p>
            When you click <b>Search</b>, the information you provided goes to our servers and matches you.
            <ul>
              <li>
                <b>age</b> is matched according to the <a target="_blank" href="http://en.wikipedia.org/wiki/Age_disparity_in_sexual_relationships#The_.22half-your-age-plus-seven.22_rule">half-plus-seven rule</a>.
              </li>
              <li>
                If you do not specify that you are <b>looking</b> for a <b>sex</b>, then those who search with that <b>sex</b> will not be paired with you. 
              </li>
              <li>
                You will only be paired with people with whom you share a <b>language</b>.
              </li>
              <li>
                <b>location</b> is not used in matching but is reported as you presented it to the other party. To rotate among options, click its button.
              </li>
            </ul>
          </p>
          <p>
            Using chat tabs, you can maintain many conversations at once.
            Similar to browser tabs, they open with
            <span class="glyphicon glyphicon-plus-sign"></span>
            and close with
            <span class="glyphicon glyphicon-plus-sign roll-plus-button ex-button"></span>. Closing a tab disconnects with the peer and deletes the conversation's content.
          </p>
          <p>
            You can send files and pictures within the messenger using
              <span class="glyphicon glyphicon-paperclip"></span>.
            Due to an issue with Firefox, non-image files cannot currently be downloaded with it.
          </p>
          <p>
            To request a video call, click
              <span class="glyphicon glyphicon-facetime-video"></span>.
            The other person will then be able to confirm or reject your request before the video starts.
            If you initiate or accept a video call and then reject app access to the camera, you will block all future camera requests as well. Fixing this is browser-specific.
        </div>
      </div>
    </div>
    <!-- Place all scripts at the end of the document so the pages load faster -->
    <!-- JQuery -->
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
  </body>
</html>
