<?php
require_once('constants.php');

function request_type_from($action_type, $meet_type) {
    return $action_type | $meet_type;
}

function action($request_type) {
    return $request_type & ACTION_BITMASK;
}

function meet($request_type) {
    return $request_type & MEET_BITMASK;
}
?>
