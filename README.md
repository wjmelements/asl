This repository used to run a website called Meerchat which was a peer-to-peer messenger in the browser supporting text, file transfer, live chat editing, and video chat. It would let you pair with strangers or with particular people. After an unsuccessful launch it was taken down and open sourced.

Developed by William, Derek, Michael and Vibhav.
Designed by Vibhav, Priya, and William.