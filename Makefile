SHELL=/bin/bash
CC=gcc
CPP=g++
INCLUDE=$(addprefix -I, include/ .)
CFLAGS=-O3 -Wno-write-strings $(INCLUDE) -fdiagnostics-color=auto -std=gnu11 -pthread -pipe -fno-exceptions -fvisibility=hidden -Wl,-O3 -Wl,--discard-all
CPPFLAGS=$(filter-out -std=gnu11,$(CFLAGS)) -std=c++11
TESTNAMES=requests match friend hash answer id_server password stranger bad_requests
TESTS=$(addprefix tst/bin/, $(TESTNAMES) )
EXECS=$(addprefix bin/, pair_server id_server)
LIBS=$(addprefix -l,stdc++)
SCRIPTGEN=include/languages.h html/languages.php
ALLCODE=include scripts src tst html *.c
MKDIRS=lib bin tst/bin html/css/min html/js/min .pass/html .pass .deploy .deploy/bin log
PHP=$(shell find html/ -name *.php)
UID=$(shell id -u)
ifeq ($(PWD), /var/www)
CSS=$(addsuffix .min.css, $(addprefix html/css/min/, cover))
JS=$(addsuffix .min.js, $(addprefix html/js/min/, diff file form title webrtc messenger video youtube textEffect.jquery))
ID_PORT=2422
PS_PORT=2423
else
CSS=
JS=$(addsuffix .min.js, $(addprefix html/js/min/, textEffect.jquery))
ID_PORT=2$(UID) 
PS_PORT=3$(UID) 
endif

.PHONY: all deploy clean distclean dist-clean again execs tests check distcheck dist-check time todo css js
all: execs tests | html/constants.php
deploy: all check execs css js status $(addprefix .deploy/, $(EXECS))
distclean dist-clean clean:
	rm -rf $(MKDIRS) tst/*.o $(SCRIPTGEN) html/constants.php
again: clean all
execs: $(EXECS)
tests: $(TESTS)
js: $(JS)
css: $(CSS)
html/css/min/%.min.css: html/css/%.css | html/css/min
	yui-compressor $< -o $@
html/js/min/%.min.js: html/js/%.js | html/js/min
	yui-compressor $< -o $@
html/js/min/textEffect.jquery.min.js: opensource/textEffect/textEffect.jquery.js | html/js/min
	yui-compressor $< -o $@
opensource/textEffect/textEffect.jquery.js: .gitmodules
	git submodule sync
	rm -rf opensource
	git submodule update --init
check: $(addprefix .pass/, $(TESTNAMES) $(PHP) html/hack) | html/constants.php
	@true
dist-check distcheck:
	@rm -rf .pass
	@make --no-print-directory check
status:
	@netstat -plnt 2>&1 | grep "$(ID_PORT)" >/dev/null || rm -f .deploy/bin/id_server
	@netstat -plnt 2>&1 | grep "$(PS_PORT)" >/dev/null || rm -f .deploy/bin/pair_server
.deploy/bin/id_server: bin/id_server | log
	@make --no-print-directory $(@D) >/dev/null
	pkill -U $(UID) -2 $< || true
	nohup $< -v -p $(ID_PORT) >> log/$(@F).log 2>&1 &
	touch $@
.deploy/bin/pair_server: bin/pair_server | log
	@make --no-print-directory $(@D) >/dev/null
	pkill -U $(UID) -2 $< || true
	nohup $< -v -p $(PS_PORT) >> log/$(@F).log 2>&1 &
	touch $@
.pass/%: tst/bin/% | .pass
	@printf "$<: "; \
	$< \
	&& echo -e "\033[0;32mpass\033[0m" && touch $@ \
	|| echo -e "\033[0;31mfail\033[0m";
.pass/html/%.php: html/%.php | .pass/html
	@hhvm -l $<
	@touch $@
.pass/html/hack: $(PHP) | .pass/html
	@hh_server --check html
	@touch $@
time: tests
	@for test in $(TESTS); do \
		echo "$$test: "; \
		time -p $$test bin/server 2>/dev/null\
		&& echo -e "\033[0;32mpass\033[0m" \
		|| echo -e "\033[0;31mfail\033[0m"; \
	done
todo:
	@grep -HIRn --color=auto "TODO\|XXX\|FIXME" $(ALLCODE)
$(MKDIRS):
	mkdir -p $@
$(SCRIPTGEN): scripts/compile_languages.py
	$< >/dev/null
.SECONDARY: tst/common.o $(TESTS)
lib/%.o: src/%.c include/%.h | lib $(SCRIPTGEN)
	$(CC) -c $(CFLAGS) $< -o $@
tst/%.o: tst/%.c
	$(CC) -c $(CFLAGS) $^ -o $@
lib/%.o: src/%.cpp include/%.h | lib $(SCRIPTGEN)
	$(CPP) -c $(CPPFLAGS) $< -o $@
tst/%.o: tst/%.c
	$(CC) -c $(CFLAGS) $^ -o $@
bin/pair_server: $(addprefix lib/, poll.o handler.o request.o reply.o friend.o match.o answer.o signals.o)
bin/id_server: $(addprefix lib/, poll.o  hash.o signals.o)
bin/%: %.c | bin
	$(CC) $(CFLAGS) $^ -o $@ $(LIBS)
tst/bin/password tst/bin/stranger tst/bin/bad_requests: bin/pair_server bin/id_server
tst/bin/id_server: bin/id_server 
tst/bin/handler: lib/poll.o lib/handler.o lib/reply.o lib/answer.o
tst/bin/match: lib/match.o
tst/bin/friend: lib/friend.o
tst/bin/hash: lib/hash.o
tst/bin/answer: lib/hash.o lib/answer.o
tst/bin/%: tst/%.c tst/common.o lib/request.o | tst/bin
	$(CC) $(CFLAGS) $(filter-out $(EXECS),$^) -o $@ $(LIBS)
tst/bin/%: tst/%.cpp | tst/bin
	$(CPP) $(CPPFLAGS) $^ -o $@
html/constants.php: bin/constants
	$< > $@
