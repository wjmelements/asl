#include "request.h"

#include <stdint.h>

struct answer_node {
    struct request* req;
    int fd;
};
// the node should be in dynamic memory
void answer_push(int64_t callee_id, struct answer_node* caller_data);
struct answer_node* answer_pop(int64_t callee_id);
int* answer_shutdown(int signal, size_t* num_unanswered_fds);
