#include <stdint.h>
#include <stdlib.h>

#include "request.h"

// search for friend, return NULL on delay, return same if already in queue
struct request_info* friend_search(struct request_info*);
// remove a request info with similar id, return ptr on success, 0 on dnf
struct request_info* friend_cancel(struct request* request);

void friend_init();
int* friend_shutdown(int signal, size_t* num_unmatched_fds);
