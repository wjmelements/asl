#include <stdint.h>
#include <stdlib.h>
#include "request.h"

/**
 * To search you must fill out this node with NULL next, with NULL prev and
 * with request and request_node in dynamic memory.
 */
struct request_node {
    struct request_info info;
    struct request_node* prev;
    struct request_node* next;
};
// search for match, return NULL on delay
struct request_node* match_search(struct request_node*);
// same as match_search but with high priority
struct request_node* match_prior(struct request_node*);
// remove a node with similar id, return 1 on success, 0 on dnf
struct request_node* match_cancel(struct request* request);

void match_init();
int* match_shutdown(int signal, size_t* num_unmatched_fds);
