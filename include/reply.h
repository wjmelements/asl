#ifndef asl_reply
#define asl_reply

#include <arpa/inet.h>

#include "request.h"

typedef enum failure_reason {
    bad_request,
    server_shutdown,
    request_timeout,
    canceled,
    not_canceled,
    already_request,
} failure_reason_t;
void print_reason(failure_reason_t);
#pragma pack(push,4)
struct reply {
    uint16_t success;
    union {
        // failure
        struct {
            failure_reason_t reason;
        };
        // success
        struct {
            // partner info
            int64_t id;
            uint16_t age;
            uint16_t male;
            /**
             * The size of the JSON string to follow
             */
            uint16_t obj_size;
        };
    };
};
#pragma pack(pop)
void reply_pair(struct reply*, struct request* pair);
void reply_failure(struct reply*, failure_reason_t);
#endif
