#ifndef asl_handler
#define asl_handler

#include "reply.h"

/*
    The handler is responsible for delivering a response to a client and
    closing the connection.
*/
void handle_async(int sockfd);
// called before any other handler activit y
void handler_init();
// shuts down the handler service
void handler_shutdown(int signal);

#endif
