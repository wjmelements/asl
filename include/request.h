#ifndef asl_request
#define asl_request
#include <stdint.h>
#include <stdlib.h>

#include "languages.h"

/*
 * It is critcal that all changes to this struct are reflected in html/structs.php
 */

// http://stackoverflow.com/questions/807244/
#define GLUE(a,b) __GLUE(a,b)
#define __GLUE(a,b) a ## b

#define CVERIFY(expr, msg) typedef char GLUE (compiler_verify_, msg) [(expr) ? (+1) : (-1)]

#define COMPILER_VERIFY(exp) CVERIFY (exp, __LINE__)

typedef enum { /* corresponds to values in constants.php */
    SEND = 0,
    ANSWER = 1,
    CANCEL = 2,
} action_type;
#define ACTION_BITMASK (SEND | ANSWER | CANCEL)


typedef enum {
    MATCH = 0 << 2,
    FRIEND = 1 << 2,
} meet_type;
#define MEET_BITMASK (MATCH | FRIEND)

// bitmasks must obviously be entirely independent
COMPILER_VERIFY(!(ACTION_BITMASK & MEET_BITMASK));

typedef uint32_t request_type;
#pragma pack(push,2)
struct request {
    request_type type;
    union {
        struct {
            // age
            uint16_t age;
            // sex
            uint16_t male;
            // looking for
            uint16_t looking_male;
            uint16_t looking_female;
            // language
            union {
                struct {
                    #define L(name) unsigned name : 1;
                    LANGUAGES
                    #undef L
                };
                struct {
                    uint64_t languages1;
                    uint64_t languages2;
                    uint64_t languages3;
                };
            };
        };
        struct {
            uint16_t password_len;
            char* password;
        };
    };
    // location
    int64_t id;
    /**
     * The request struct is followed by a JSON string of variable size
     */
    uint16_t obj_size;
};
#pragma pack(pop)

struct request_info {
    struct request* request;
    int fd;
    char* obj;
};

request_type request_type_from(action_type, meet_type);
action_type action(request_type);
meet_type meet(request_type);

// parse a request from the string, return NULL on invalid
struct request* request_from(unsigned char* buffer);
int request_valid(const struct request*);
int requests_compatible(const struct request*, const struct request*);

#endif
