// async closes a socket
void close_async(int sockfd);
// awaits all closes
void wait_all(void);
