#include "reply.h"

#include <stdio.h>
#include <string.h>

void print_reason(failure_reason_t reason) {
    fprintf(stderr, "Request failed: ");
    switch(reason) {
        case bad_request:
            fprintf(stderr, "Invalid request");
            break;
        case server_shutdown:
            fprintf(stderr, "Server shutting down");
            break;
        case request_timeout:
            fprintf(stderr, "Request timed out");
            break;
        case canceled:
            fprintf(stderr, "Canceled");
            break;
        case not_canceled:
            fprintf(stderr, "Not canceled");
            break;
        case already_request:
            fprintf(stderr, "Already requested");
            break;
    }
    fputc('\n', stderr);
}
void reply_failure(struct reply* rep, failure_reason_t reason) {
    memset(rep,0,sizeof(struct reply));
    rep->success = 0;
    rep->reason = reason;
}
void reply_pair(struct reply* rep, struct request* pair) {
    memset(rep,0,sizeof(struct reply));
    rep->success = 1;
    rep->id = pair->id;
    rep->age = pair->age;
    rep->male = pair->male;
    rep->obj_size = pair->obj_size;
}
