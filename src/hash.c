#include "hash.h"

#include <errno.h>
#include <pthread.h>
#include <stdio.h>
#include <stdlib.h>

// does not need to be initialized but cannot start with 0
static uint64_t current = 1;

static pthread_mutex_t hash_mutex = PTHREAD_MUTEX_INITIALIZER;

/**
 * Generates the next number in the cycle
 * Thread-safe
 */
static uint64_t get_next_curr(void) {
    if (pthread_mutex_lock(&hash_mutex)) {
        perror("pthread_mutex_lock");
        exit(errno);
    }
    current ^= (current << 21);
    current ^= (current >> 35);
    current ^= (current << 4);
    uint64_t copy = current;
    if (pthread_mutex_unlock(&hash_mutex)) {
        perror("phtread_mutex_unlock");
        exit(errno);
    }
    return copy;
}
const uint64_t prime = 1361;
int64_t get_hash() {
    uint64_t curr = get_next_curr();
    return curr * prime;
}
