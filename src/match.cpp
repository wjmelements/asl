extern "C" {
    #include "match.h"
    #include "request.h"
}

#include <errno.h>
#include <pthread.h>
#include <stdio.h>

#include <unordered_map>
using std::unordered_map;

extern int VERBOSE;

typedef enum {
    F_F,
    F_MF,
    F_M,
    M_F,
    M_MF,
    M_M,
    PF_F,
    PF_MF,
    PF_M,
    PM_F,
    PM_MF,
    PM_M,
    POOL_SIZE
} pool;

// linked lists of unmatched requests
struct request_node* unmatched_heads[POOL_SIZE];
struct request_node* unmatched_tails[POOL_SIZE];
// map for quick cancellation
unordered_map<int64_t, struct request_node*> unmatched_ids;

// index of request in the linked list array
static int unmatched_index_of(struct request* request, int priority) {
    return 6 * priority + 3 * request->male + request->looking_male - request->looking_female + 1;
}

// mutex for shared memory
pthread_mutex_t match_mutex;

// called before any operation
void match_init() {
    for (int i = 0; i < 6; i++) {
        unmatched_tails[i] = unmatched_heads[i] = NULL;
    }
    if (pthread_mutex_init(&match_mutex, NULL)) {
        perror("pthread_mutex_init");
        exit(errno);
    }
}

// shutdown match service, return disappointed fds
int* match_shutdown(int signal, size_t* num_unmatched_fds) {
    if (VERBOSE) {
        fprintf(stderr, "match: shutting down\n");
    }
    size_t buffer_size = 6 * sizeof(int);
    int* buffer = (int*) malloc(sizeof(int) * buffer_size);
    *num_unmatched_fds = 0;
    pthread_mutex_lock(&match_mutex);
    for (int i = 0; i < 6; i++) {
        struct request_node* curr = unmatched_heads[i];
        while (curr) {
            if (*num_unmatched_fds == buffer_size) {
                buffer_size *= 2;
                buffer = (int*) realloc(buffer, sizeof(int) * buffer_size);
            }
            buffer[(*num_unmatched_fds)++] = curr->info.fd;
            struct request_node* const prev = curr;
            curr = curr->next;
            free(prev->info.request);
            free(prev->info.obj);
            free(prev);
        }
    }
    // TODO not necessary except for testing
    unmatched_ids.clear();
    if (pthread_mutex_unlock(&match_mutex)) {
        perror("pthread_mutex_unlock");
    }
    if (pthread_mutex_destroy(&match_mutex)) {
        perror("pthread_mutex_destroy");
    }
    if (VERBOSE) {
        fprintf(stderr, "match: %lu unmatched found\n", *num_unmatched_fds);
    }
    return buffer;
}

static void link_node(struct request_node* to_add, int index) {
    to_add->prev = unmatched_tails[index];
    if (to_add->prev) {
        to_add->prev->next = to_add;
    }
    if (!unmatched_heads[index]) {
        unmatched_heads[index] = to_add;
    }
    unmatched_tails[index] = to_add;
    unmatched_ids[to_add->info.request->id] = to_add;
}

static void unlink_ends(struct request_node* to_rm, int index) {
    if (to_rm == unmatched_tails[index]) {
        unmatched_tails[index] = to_rm->prev;
    }
    if (to_rm == unmatched_heads[index]) {
        unmatched_heads[index] = to_rm->next;
    }
}

static void unlink_node(struct request_node* to_rm, int index) {
    if (to_rm->prev) {
        to_rm->prev->next = to_rm->next;
    }
    if (to_rm->next) {
        to_rm->next->prev = to_rm->prev;
    }
    unlink_ends(to_rm, index);
}

// look in linked list corresponding to index for pair, return NULL on sadness
static struct request_node* pair(const struct request* seeker, int index) {
    struct request_node* curr = unmatched_heads[index];
    while (curr) {
        if (requests_compatible(curr->info.request, seeker)) {
            unlink_node(curr, index);
            return curr;
        }
        curr = curr->next;
    }
    return NULL;
}

struct request_node* match_cancel(struct request* request) {
    if (pthread_mutex_lock(&match_mutex)) {
        perror("pthread_mutex_lock");
    }
    auto iterator = unmatched_ids.find(request->id);
    if (iterator != unmatched_ids.end()) {
        struct request_node* to_rm = iterator->second;
        int index = unmatched_index_of(request, 0);
        unlink_node(to_rm, index);
        unlink_ends(to_rm, index + 6);
        unmatched_ids.erase(iterator);
        pthread_mutex_unlock(&match_mutex);
        return to_rm;
    }
    pthread_mutex_unlock(&match_mutex);
    // we didn't find it oh well
    return NULL;
}

static inline request_node* search_with_prior(struct request* request, int priority) {
    static request_node* found;
    int offset = 6 * priority;
    if (request->looking_male) {
        found = pair(request, M_MF + offset);
        if (found) {
            return found;
        }
    }
    if (request->looking_female) {
        found = pair(request, F_MF + offset);
        if (found) {
            return found;
        }
    }
    if (request->male) {
        if (request->looking_male) {
            found = pair(request, M_M + offset);
            if (found) {
                return found;
            }
        }
        if (request->looking_female) {
            found = pair(request, F_M + offset);
            if (found) {
                return found;
            }
        }
    } else {
        if (request->looking_male) {
            found = pair(request, M_F + offset);
            if (found) {
                return found;
            }
        }
        if (request->looking_female) {
            found = pair(request, F_F + offset);
            if (found) {
                return found;
            }
        }
    }
    return NULL;
}

static struct request_node* search(struct request* request) {
    struct request_node* ret = NULL;
    ret = search_with_prior(request, 1);
    if (ret) {
        return ret;
    }
    ret = search_with_prior(request, 0);
    return ret;
}

static struct request_node* match_request(struct request_node* seeker_node, int priority) {
    if (pthread_mutex_lock(&match_mutex)) {
        perror("pthread_mutex_lock");
    }
    if (unmatched_ids.count(seeker_node->info.request->id)) {
        if (pthread_mutex_unlock(&match_mutex)) {
            perror("pthread_mutex_unlock");
        }
        return seeker_node;
    }
    struct request* request = seeker_node->info.request;
    struct request_node* found = search(request);
    if (found) {
        unmatched_ids.erase(found->info.request->id);
        pthread_mutex_unlock(&match_mutex);
        return found;
    }
    // no match found
    int index = unmatched_index_of(seeker_node->info.request, priority);
    link_node(seeker_node, index);
    pthread_mutex_unlock(&match_mutex);
    return NULL;
}

struct request_node* match_search(struct request_node* seeker_node) {
    return match_request(seeker_node, /* PRIORITY */ 0);
}

struct request_node* match_prior(struct request_node* seeker_node) {
    return match_request(seeker_node, /* PRIORITY */ 1);
}
