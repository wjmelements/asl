#include "answer.h"
#include "handler.h"
#include "match.h"
#include "friend.h"
#include "poll.h"
#include "reply.h"
#include "request.h"

#include <errno.h>
#include <pthread.h>
#include <sched.h>
#include <signal.h>
#include <stdatomic.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/socket.h>
#include <sys/time.h>
#include <unistd.h>

#ifndef NULL
#define NULL 0
#endif

extern int VERBOSE;

// on failure, replies to client
void handle_failure(failure_reason_t reason, int sockfd) {
    struct reply rep;
    reply_failure(&rep, reason);
    if (VERBOSE) {
        print_reason(reason);
    }
    if (send(sockfd, &rep, sizeof(struct reply), 0) == -1) {
        perror("send");
    }
    close_async(sockfd);
}

void handle_caller(struct request_info* caller, int64_t callee_id) {
    struct answer_node* caller_node = malloc(sizeof(struct answer_node));
    caller_node->req = caller->request;
    caller_node->fd = caller->fd;
    answer_push(callee_id, caller_node);
}

void handle_callee(struct request_info* callee, struct request_info* caller) {
    struct reply rep;
    reply_pair(&rep, caller->request);
    if (send(callee->fd, &rep, sizeof(struct reply), 0) == -1) {
        perror("send");
        exit(errno);
    }
    if (send(callee->fd, caller->obj, rep.obj_size, 0) == -1) {
        perror("send");
        exit(errno);
    }
    close_async(callee->fd);
}

// returns the request_info that is the callee
void handle_caller_callee(struct request_info* request_info, struct request_info* partner_info) {
    // the callee has the higher id
    struct request_info* callee_info = (
        partner_info->request->id > request_info->request->id
        ? partner_info
        : request_info
    );
    // TODO there is a bittrick for this
    struct request_info* caller_info = (
        request_info == callee_info
        ? partner_info
        : request_info
    );
    handle_callee(callee_info, caller_info);
    handle_caller(caller_info, callee_info->request->id);
    if (meet(callee_info->request->type) == FRIEND) {
        // can free both passwords
        free(caller_info->request->password);
        free(callee_info->request->password);
    }
    free(callee_info->request);
    // can free both objects
    free(callee_info->obj);
    free(caller_info->obj);
}

void handle(char* req_bytes, int sockfd) {
    struct request* const req = request_from(req_bytes);
    
    if (!request_valid(req)) {
        if (VERBOSE) {
            fprintf(stderr, "Received invalid request\n");
        }
        free(req);
        handle_failure(bad_request, sockfd);
        return;
    }
    if (meet(req->type) == FRIEND && action(req->type) == SEND) {
        // read the password
        char* password = malloc(req->password_len + 1);
        ssize_t pwd_recvd;
        for (int i = 0; i < 3; i++) {
            pwd_recvd = recv(sockfd, password, req->password_len, 0);
            if (pwd_recvd == -1) {
                switch(errno) {
                case EAGAIN:
                    if (VERBOSE) {
                        perror("recv:send:obj:EAGAIN");
                    }
                    continue;
                default:
                    perror("recv:send:password");
                    exit(errno);
                }
                break;
            } else if (pwd_recvd == 0) {
                // they hung up
                if (VERBOSE) {
                    fprintf(stderr, "Unexpected hangup in password.\n");
                }
                free(password);
                free(req);
                close_async(sockfd);
                return;
            } else if (pwd_recvd != req->password_len) {
                // invalid size
                fprintf(stderr, "Incorrent password size, expected %u, got %zd\n",
                  req->password_len, pwd_recvd);
                free(password);
                free(req);
                handle_failure(bad_request, sockfd);
                return;
            }
            break;
        }
        if (pwd_recvd == -1) {
            free(password);
            free(req);
            handle_failure(request_timeout, sockfd);
            return;
        }
        password[req->password_len] = '\0';
        req->password = password;
    }
    if (action(req->type) == SEND) {
        if (VERBOSE) {
            fprintf(stderr, "Request type: send\n");
        }
        char* obj = malloc(req->obj_size + 1);
        ssize_t obj_recvd;
        for (int i = 0; i < 3; i++) {
            obj_recvd = recv(sockfd, obj, req->obj_size + 1, 0);
            if (obj_recvd == -1) {
                switch(errno) {
                case EAGAIN:
                    if (VERBOSE) {
                        perror("recv:send:obj:EAGAIN");
                    }
                    continue;
                default:
                    perror("recv:send:obj");
                    exit(errno);
                }
            } else if (obj_recvd == 0) {
                // they hung up
                if (VERBOSE) {
                    fprintf(stderr, "Unexpected hangup in object.\n");
                }
                free(obj);
                if (meet(req->type) == FRIEND && action(req->type) == SEND) {
                    free(req->password);
                }
                free(req);
                close_async(sockfd);
                return;
            } else if (obj_recvd != req->obj_size) {
                // invalid size
                if (VERBOSE) {
                    fprintf(stderr, "Invalid object size: %zd/%u\n", obj_recvd, req->obj_size);
                }
                free(obj);
                if (meet(req->type) == FRIEND && action(req->type) == SEND) {
                    free(req->password);
                }
                free(req);
                handle_failure(bad_request, sockfd);
                return;
            }
            break;
        }
        if (obj_recvd == -1) {
            free(obj);
            if (meet(req->type) == FRIEND && action(req->type) == SEND) {
                free(req->password);
            }
            free(req);
            handle_failure(request_timeout, sockfd);
            return;
        }
        if (meet(req->type) == MATCH) {
            struct request_node* request_node =
              malloc(sizeof(struct request_node));
            request_node->info.fd = sockfd;
            request_node->info.request = req;
            request_node->next = NULL;
            request_node->prev = NULL;
            request_node->info.obj = obj;
            struct request_node* partner_node = match_search(request_node);
            if (partner_node == request_node) {
                // they sent another request while still have one; forbid this
                handle_failure(already_request, request_node->info.fd);
                free(request_node->info.request);
                free(request_node);
            } else {
                if (partner_node) {
                    // reply
                    handle_caller_callee(
                        &request_node->info,
                        &partner_node->info
                    );
                    // can free both nodes
                    free(request_node);
                    free(partner_node);
                } else {
                    if (VERBOSE) {
                        fprintf(stderr, "Not yet matched\n");
                    }
                }
            }
        } else { // type FRIEND
            struct request_info* request_info =
              malloc(sizeof(*request_info));
            request_info->fd = sockfd;
            request_info->request = req;
            request_info->obj = obj;
            struct request_info* partner_info = friend_search(request_info);
            if (partner_info == request_info) {
                // is already in queue, forbid this
                // TODO memcheck this out
                handle_failure(already_request, request_info->fd);
                free(request_info->request->password);
                free(request_info->request);
                free(request_info);
            } else {
                if (partner_info) {
                    // reply
                    handle_caller_callee(request_info, partner_info);
                    free(request_info);
                    free(partner_info);
                } else {
                    if (VERBOSE) {
                        fprintf(stderr, "Not yet matched\n");
                    }
                }
            }
        }
    } else if (action(req->type) == CANCEL) {
        // we want to reply to cancel before send but first we have to know what to say
        // this is because if they close the browser window they are URGENTLY
        // waiting on the reply from cancel.php
        // also UI is waiting for the cancel reply and not the send reply
        int send_fd = 0;
        if (VERBOSE) {
            fprintf(stderr, "Request type: cancel\n");
        }
        if (meet(req->type) == MATCH) {
            struct request_node* node = match_cancel(req);
            if (node) {
                if (VERBOSE) {
                    fprintf(stderr, "cancel: found in match service\n");
                }
                send_fd = node->info.fd;
                free(node->info.obj);
                free(node->info.request);
                free(node);
            } else {
                // it wasn't in match, so check answer map
                struct answer_node* node = answer_pop(req->id);
                if (node) {
                    if (VERBOSE) {
                        fprintf(stderr, "cancel: found in answer service\n");
                    }
                    send_fd = node->fd;
                    free(node->req);
                    free(node);
                } else {
                    send_fd = -1;
                }
            }
        } else { // type FRIEND
            struct request_info* info = friend_cancel(req);
            if (info) {
                if (VERBOSE) {
                    fprintf(stderr, "cancel: found in friend service\n");
                }
                send_fd = info->fd;
                free(info->obj);
                free(info->request->password);
                free(info->request);
                free(info);
            } else {
                struct answer_node* node = answer_pop(req->id);
                if (node) {
                    if (VERBOSE) {
                        fprintf(stderr, "cancel: found in answer service\n");
                    }
                    send_fd = node->fd;
                    free(node->req);
                    free(node);
                } else {
                    send_fd = -1;
                }
            }
        }
        if (send_fd == -1) {
            if (VERBOSE) {
                fprintf(stderr, "cancel: did not find\n");
            }
            handle_failure(not_canceled, sockfd);
        } else {
            // reply to cancel request first
            struct reply rep;
            reply_pair(&rep, req);
            if (send(sockfd, &rep, sizeof(struct reply), 0) == -1) {
                perror("send");
                exit(errno);
            }
            close_async(sockfd);
            // reply to send request
            handle_failure(canceled, send_fd);
        }
        free(req);
    } else if (action(req->type) == ANSWER) {
        if (VERBOSE) {
            fprintf(stderr, "Request type: answer\n");
        }
        char* obj = (char*) malloc(req->obj_size);
        ssize_t obj_recvd = recv(sockfd, obj, req->obj_size, 0);
        if (obj_recvd == -1) {
            switch(errno) {
            case EAGAIN:
                // TODO don't actually want to exit here
            default:
                perror("recv:ans:obj");
                exit(errno);
            }
        } else if (obj_recvd == 0) {
            // they hung up
            if (VERBOSE) {
                fprintf(stderr, "Unexpected hangup in answer object.\n");
            }
            free(obj);
            free(req);
            close_async(sockfd);
            return;
        } else if (obj_recvd != req->obj_size) {
            // invalid size
            fprintf(stderr, "Invalid object size: %zd/%u\n", obj_recvd, req->obj_size);
            free(obj);
            free(req);
            handle_failure(bad_request, sockfd);
            return;
        }
        struct answer_node* node = answer_pop(req->id);
        if (node) {
            // reply to caller
            struct reply rep;
            reply_pair(&rep, req);
            if (send(node->fd, &rep, sizeof(struct reply), 0) == -1) {
                perror("send");
                exit(errno);
            }
            if (send(node->fd, obj, req->obj_size, 0) == -1) {
                perror("send");
                exit(errno);
            }
            close_async(node->fd);
            // reply to callee (answerer)
            reply_pair(&rep, req);
            rep.obj_size = 0;
            if (send(sockfd, &rep, sizeof(struct reply), 0) == -1) {
                perror("send");
                exit(errno);
            }
            close_async(sockfd);
            free(obj);
            free(node->req);
            free(node);
            free(req);
        } else {
            if (VERBOSE) {
                fprintf(stderr, "Caller canceled\n");
            }
            // reply to answerer
            handle_failure(canceled, sockfd);
        }
    } else {
        handle_failure(bad_request, sockfd);
    }
}

struct handler_receiver_arg {
    int socketfd;
};
// thread to receive response
void* handler_receiver(void* arg) {
    const struct handler_receiver_arg* const hr_arg = arg;
    const int socketfd = hr_arg->socketfd;
    struct timeval tv;
    tv.tv_sec = 1;
    tv.tv_usec = 0;
    setsockopt(socketfd, SOL_SOCKET, SO_RCVTIMEO, &tv, sizeof(struct timeval));
    const size_t rec_size = sizeof(struct request);
    char request[rec_size];
    // receive request
    ssize_t recv_ret = recv(socketfd,request,rec_size,0);
    switch (recv_ret) {
    case sizeof(struct request):
        handle(request, socketfd);
        break;
    case -1:
        switch (errno) {
        case EAGAIN:
            handle_failure(request_timeout, socketfd);
            break;
        default:
            perror("recv:request");
        }
        break;
    default:
        if (VERBOSE) {
            fprintf(stderr, "Bad request size: %lu\n", recv_ret);
        }
        handle_failure(bad_request, socketfd);
    }
    free(arg);
    return NULL;
}
void handle_async(int socketfd) {
    pthread_t child;
    struct handler_receiver_arg* arg = 
      malloc(sizeof(struct handler_receiver_arg));
    arg->socketfd = socketfd;
    pthread_create(&child, NULL, handler_receiver, arg);
    pthread_detach(child);
}

void handler_init() {
    if (VERBOSE) {
        fprintf(stderr, "handler: init\n");
    }
    match_init();
    friend_init();
}
void handle_shutdown_fds(int signal, int* fds, size_t count) {
    switch (signal) {
        case SIGINT:
        case SIGABRT:
        case SIGQUIT:
            for (size_t i = 0; i < count; i++) {
                print_reason(server_shutdown);
                handle_failure(server_shutdown, fds[i]);
            }
            break;
    }
}
void handler_shutdown(int signal) {
    if (VERBOSE) {
        fprintf(stderr, "handler: shutdown\n");
    }
    size_t count;
    int* unmatched_fds = match_shutdown(signal, &count);
    
    handle_shutdown_fds(signal, unmatched_fds, count);
    free(unmatched_fds);
    int* unanswered_fds = answer_shutdown(signal, &count);
    handle_shutdown_fds(signal, unanswered_fds, count);
    free(unanswered_fds);
    wait_all();
    if (VERBOSE) {
        fprintf(stderr, "handler: finished shutdown\n");
    }
}
