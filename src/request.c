#include "request.h"

#include <string.h>
#include <stdlib.h>
#include <stdio.h>

#ifndef max
#define max(x,y) (x > y ? x : y)
#endif
#ifndef min
#define min(x,y) (x < y ? x : y)
#endif

extern int VERBOSE;

request_type request_type_from(action_type a, meet_type m) {
    return a | m;
}
action_type action(request_type req_type) {
    return req_type & ACTION_BITMASK;
}
meet_type meet(request_type req_type) {
    return req_type & MEET_BITMASK;
}

struct request* request_from(unsigned char* buffer) {
    struct request* ret = (struct request*) malloc(sizeof(struct request));
    memcpy(ret, buffer, sizeof(struct request));
    return ret;
}
int request_type_valid(const request_type req_type) {
    if (req_type ^ (req_type & (ACTION_BITMASK | MEET_BITMASK))) {
        if (VERBOSE) {
            fprintf(stderr, "Invalid request type %i\n", req_type);
        }
        return 0;
    }
    action_type a = action(req_type);
    if (a != SEND && a != ANSWER && a != CANCEL) {
        if (VERBOSE) {
            fprintf(stderr, "Invalid action %i\n", a);
        }
        return 0;
    }
    meet_type m = meet(req_type);
    if (m != MATCH && m != FRIEND) {
        if (VERBOSE) {
            fprintf(stderr, "Invalid meet %i\n", m);
        }
        return 0;
    }
    return 1;
}
int request_valid(struct request const *const req) {
    //fprintf(stderr, "evaluating request with size %u age %u\n", req->obj_size, req->age);
    return request_type_valid(req->type)
        && (!req->obj_size != (action(req->type) != CANCEL))
        && (req->obj_size <= 25000)
        && ((meet(req->type) == MATCH) ?  (
            req->age >= 14 && req->age <= 120
            && (req->looking_male || req->looking_female)
            && (req->languages1 || req->languages2 || req->languages3)
        ) : ( // FRIEND
            req->password_len < 256
        ))
    ;
}
int requests_compatible(const struct request* one, const struct request* two) {
    uint16_t older = max(one->age, two->age);
    uint16_t younger = min(one->age, two->age);
    return (older / 2 + 7 <= younger) 
        && ((one->male && two->looking_male) || (!one->male && two->looking_female))
        && ((two->male && one->looking_male) || (!two->male && one->looking_female))
        && ((one->languages1 & two->languages1)
            || (one->languages2 & two->languages2)
            || (one->languages3 & two->languages3));
}
