#include <errno.h>
#include <signal.h>
#include <stdio.h>
#include <stdlib.h>

// Signals to block during shutdown and to trigger shutdown
#define NUM_HAPPY_SHUTDOWN_SIGNALS 7
const int HAPPY_SHUTDOWN_SIGNALS[NUM_HAPPY_SHUTDOWN_SIGNALS] = {
    SIGINT,
    SIGTERM,
    SIGQUIT,
    SIGABRT,
    SIGHUP,
    SIGFPE,
    SIGSYS
};
#define NUM_SIGNALS_TO_IGNORE 1
const int SIGNALS_TO_IGNORE[NUM_SIGNALS_TO_IGNORE] = {
    SIGPIPE
};
void set_signal_actions(void (*shutdown)(int)) {
    struct sigaction term_act;
    sigemptyset(&term_act.sa_mask);
    for (int i = 0; i < NUM_HAPPY_SHUTDOWN_SIGNALS; i++) {
        sigaddset(&term_act.sa_mask, HAPPY_SHUTDOWN_SIGNALS[i]);
    }
    for (int i = 0; i < NUM_SIGNALS_TO_IGNORE; i++) {
        sigaddset(&term_act.sa_mask, SIGNALS_TO_IGNORE[i]);
    }
    term_act.sa_flags = 0;
    term_act.sa_handler = shutdown;
    for (int i = 0; i < NUM_HAPPY_SHUTDOWN_SIGNALS; i++) {
        if (sigaction(HAPPY_SHUTDOWN_SIGNALS[i], &term_act, NULL)) {
            perror("sigaction");
            exit(errno);
        }
    }
    term_act.sa_handler = SIG_IGN;
    for (int i = 0; i < NUM_SIGNALS_TO_IGNORE; i++) {
        if (sigaction(SIGNALS_TO_IGNORE[i], &term_act, NULL)) {
            perror("sigaction");
            exit(errno);
        }
    }
}
