#include "poll.h" 

#include <errno.h>
#include <pthread.h>
#include <stdatomic.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <unistd.h>

extern int VERBOSE;

volatile int pending = 0;
// waits for the client to close their connection
void* handler_poller(void* arg) {
    int fd = *(int*)arg;
    free(arg);
    // increase timeout
    struct timeval tv;
    tv.tv_sec = 2;
    tv.tv_usec = 0;
    setsockopt(fd, SOL_SOCKET, SO_RCVTIMEO, &tv, sizeof(struct timeval));
    // buffer in which to read
    char buffer[100];
    // number of extra double words to tolerate
    size_t tolerate = 10;
    while (1) {
        ssize_t peek = recv(fd, buffer, 100, MSG_PEEK);
        if (peek == 0) {
            // client has disconnected
            close(fd);
            atomic_fetch_sub(&pending, 1);
            return NULL;
        } else if (peek == -1) {
            switch (errno) {
            case EAGAIN:
                if (tolerate) {
                    if (VERBOSE) {
                        fprintf(stderr, "Tolerating EAGAIN (%lu)\n", tolerate);
                    }
                    peek = recv(fd, buffer, 8, 0);
                    tolerate--;
                    continue;
                }
            default:
                perror("recv:async");
                close(fd);
                atomic_fetch_sub(&pending, 1);
                return &errno;
            }
        } else {
            // client has sent more bytes
            if (tolerate) {
                if (VERBOSE) {
                    fprintf(stderr, "Tolerating additional bytes (%lu)\n", tolerate);
                }
                // tolerate a few
                peek = recv(fd, buffer, 8, 0);
                tolerate--;
            } else {
                // fuck them
                fprintf(stderr, "Ignoring extra bytes\n");
                close(fd);
                atomic_fetch_sub(&pending, 1);
                return NULL;
            }
        }
        sched_yield();
    }
}

// Eventually closes the connection
// NOTE: we could close immediately but this would cause ADDR_IN_USE issues
void close_async(int sockfd) {
    pthread_t thread;
    int* fd = malloc(sizeof(int));
    *fd = sockfd;
    pthread_create(&thread, NULL, handler_poller, fd);
    pthread_detach(thread);
    atomic_fetch_add(&pending, 1);
}
void wait_all(void) {
    // wait for threads to close async
    size_t count = 0;
    while (pending > 0) {
        if (count > 160 && (count % 20 == 0)) {
            if (VERBOSE) {
                fprintf(stderr,"pending: %i\n", pending);
            }
            count = 0;
        } else {
            count++;
        }
        sched_yield();
    }
    if (pending < 0) {
        fprintf(stderr, "pending: %i\n", pending);
    }
}
