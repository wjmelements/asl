extern "C" {
    #include "friend.h"
    #include "request.h"
}

#include <errno.h>
#include <pthread.h>
#include <stdio.h>
#include <string>
using std::string;
#include <unordered_map>
using std::unordered_map;

extern int VERBOSE;

// map of passwords to requests
unordered_map<string, struct request_info*> password_map;
unordered_map<int64_t, struct request_info*> id_map;

// mutex for shared memory
pthread_mutex_t friend_mutex;

// called before any operation
void friend_init() {
    if (pthread_mutex_init(&friend_mutex, NULL)) {
        perror("pthread_mutex_init");
        exit(errno);
    }
}

// shutdown match service, return disappointed fds
int* friend_shutdown(int signal, size_t* num_unmatched_fds) {
    if (VERBOSE) {
        fprintf(stderr, "friend: shutting down\n");
    }
    size_t buffer_size = 6 * sizeof(int);
    int* buffer = (int*) malloc(sizeof(int) * buffer_size);
    *num_unmatched_fds = 0;
    pthread_mutex_lock(&friend_mutex);
    for(auto iter = password_map.begin(); iter != password_map.end(); iter++) {
        struct request_info* curr = iter->second;
        if (*num_unmatched_fds == buffer_size) {
            buffer_size *= 2;
            buffer = (int*) realloc(buffer, sizeof(int) * buffer_size);
        }
        buffer[(*num_unmatched_fds)++] = curr->fd;
    }
    if (pthread_mutex_unlock(&friend_mutex)) {
        perror("pthread_mutex_unlock");
    }
    if (pthread_mutex_destroy(&friend_mutex)) {
        perror("pthread_mutex_destroy");
    }
    if (VERBOSE) {
        fprintf(stderr, "friend: %lu unmatched found\n", *num_unmatched_fds);
    }
    return buffer;
}

/*
 *  Returns the number of requests that are in the password map.
 */
int num_friend_requests(void) {
    return password_map.size();
}
struct request_info* friend_cancel(struct request* request) {
    if (pthread_mutex_lock(&friend_mutex)) {
        perror("pthread_mutex_lock");
    }
    auto iterator = id_map.find(request->id);
    if (iterator != id_map.end()) {
        struct request_info* to_rm = iterator->second;
        string key (to_rm->request->password, to_rm->request->password_len);
        password_map.erase(key);
        id_map.erase(iterator);
        pthread_mutex_unlock(&friend_mutex);
        return to_rm;
    }
    if (pthread_mutex_unlock(&friend_mutex)) {
        perror("pthread_mutex_unlock");
    }
    // we didn't find it oh well
    return 0;
}
struct request_info* friend_search(struct request_info* seeker_info) {
    struct request *request = seeker_info->request;
    string password_str(request->password, request->password_len);
    if (pthread_mutex_lock(&friend_mutex)) {
        perror("pthread_mutex_lock");
    }
    if (id_map.count(request->id)) {
        // is already searching, forbid this
        if (pthread_mutex_unlock(&friend_mutex)) {
            perror("pthread_mutex_unlock");
        }
        return seeker_info;
    }
    auto password_iterator = password_map.find(password_str);
    if (password_iterator != password_map.end()) {
        struct request_info* found = password_iterator->second;
        id_map.erase(found->request->id);
        password_map.erase(password_iterator);
        if (pthread_mutex_unlock(&friend_mutex)) {
            perror("pthread_mutex_unlock");
        }
        return found;
    }
    // no match found
    id_map[request->id] = seeker_info;
    password_map[password_str] = seeker_info;
    if (pthread_mutex_unlock(&friend_mutex)) {
        perror("pthread_mutex_unlock");
    }
    return NULL;
}
