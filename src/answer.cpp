#include <map>
using std::map;
#include <pthread.h>

extern "C" {
    #include "answer.h"
}

extern int VERBOSE;

static map<int64_t, struct answer_node*> answer_map;
pthread_mutex_t answer_mutex = PTHREAD_MUTEX_INITIALIZER;

void answer_push(int64_t id, struct answer_node* node) {
    pthread_mutex_lock(&answer_mutex);
    answer_map[id] = node;
    pthread_mutex_unlock(&answer_mutex);
    if (VERBOSE) {
        fprintf(stderr, "Pushing %p for %li\n", node, id);
    }
}
struct answer_node* answer_pop(int64_t id) {
    struct answer_node* ret;
    pthread_mutex_lock(&answer_mutex);
    auto iterator = answer_map.find(id);
    if (iterator == answer_map.end()) {
        ret = NULL;
    } else {
        ret = (*iterator).second;
        answer_map.erase(iterator);
    }
    pthread_mutex_unlock(&answer_mutex);
    if (VERBOSE) {
        fprintf(stderr, "Popping %p for %li\n", ret, id);
    }
    return ret;
}

int* answer_shutdown(int signal, size_t* num_unanswered_fds) {
    if (VERBOSE) {
        fprintf(stderr, "answer: shutting down\n");
    }
    size_t buffer_size = 6 * sizeof(int);
    int* buffer = (int*) malloc(sizeof(int) * buffer_size);
    *num_unanswered_fds = 0;
    pthread_mutex_lock(&answer_mutex);
    for (auto it = answer_map.cbegin(); it != answer_map.cend(); it++) {
        if (*num_unanswered_fds == buffer_size) {
            buffer_size *= 2;
            buffer = (int*) realloc(buffer, sizeof(int) * buffer_size);
        }
        struct answer_node* node = (*it).second;
        buffer[(*num_unanswered_fds)++] = node->fd;
        free(node->req);
        // do not need to free node because c++ destructor?
    }
    pthread_mutex_unlock(&answer_mutex);
    pthread_mutex_destroy(&answer_mutex);
    return buffer;
}
