#ifndef _POSIX_SOURCE
#define _POSIX_SOURCE
#endif

#include <arpa/inet.h>
#include <errno.h>
#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <unistd.h>

#include "handler.h"
#include "hash.h"
#include "poll.h"
#include "signals.h"

#define BACKLOG 256
char usage[360] = "\
Usage: id_server [-p PORTNUM]\n\
    -v verbose\n\
    -p specify listening port\n\
";
void print_usage(void) {
    fprintf(stderr,"%s", usage);
}
int PORTNUM = 2422;
int VERBOSE = 0;
void parse_args(int argc, char* argv[]) {
    for (int i = 1; i < argc; i++) {
        char* arg = argv[i];
        if (arg[0] == '-') {
            switch(arg[1]) {
                case 'p': // PORTNUM
                    i++; // next arg is PORTNUM
                    if (i >= argc) {
                        print_usage();
                        exit(1);
                    }
                    char* portnum = argv[i];
                    PORTNUM = atoi(portnum);
                    break;
                case 'v': // verbose
                    VERBOSE = 1;
                    break;
                default:
                    print_usage();
                    exit(1);
            }
        } else {
            print_usage();
            exit(1);
        }
    }
}

int in_socketfd;

void id_shutdown(int signal) {
    if (VERBOSE) {
        fprintf(stderr, "Received signal %i\n", signal);
    }
    wait_all();
    if (close(in_socketfd)) {
        perror("close");
        exit(errno);
    }
    exit(EXIT_SUCCESS);
}

int main(int argc, char* argv[]) {
    parse_args(argc, argv);
    set_signal_actions(id_shutdown);
    // set up socket to listen
    in_socketfd = socket(AF_INET, SOCK_STREAM, 0);
    if (in_socketfd == -1) {
        perror("socket");
        exit(errno);
    }
    struct sockaddr_in in_addr;
    memset(&in_addr,0,sizeof(in_addr));
    in_addr.sin_family = AF_INET;
    in_addr.sin_addr.s_addr = htonl(INADDR_ANY);
    in_addr.sin_port = htons(PORTNUM);
    if (bind(in_socketfd,(struct sockaddr*)&in_addr,sizeof(struct sockaddr))) {
        perror("bind");
        exit(errno);
    }
    if (listen(in_socketfd,BACKLOG)) {
        perror("listen");
        exit(errno);
    }
    while (1) {
        // accept connection
        int socketfd = accept(in_socketfd, NULL, NULL);
        if (socketfd == -1) {
            perror("accept");
            exit(errno);
        }
        uint64_t hash = get_hash();
        if (VERBOSE) {
            fprintf(stderr, "sending %lu\n", hash);
        }
        send(socketfd, &hash, sizeof(uint64_t), 0);
        close_async(socketfd);
    }
    printf("IMPOSSIBLE");
}
