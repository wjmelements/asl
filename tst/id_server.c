#ifndef _POSIX_SOURCE
#define _POSIX_SOURCE
#endif

#include <arpa/inet.h>
#include <assert.h>
#include <errno.h>
#include <sched.h>
#include <signal.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <unistd.h>

#include "reply.h"
#include "tst/common.h"

// port number for id_server
#define PORTNUM 2097

int client() {
    int sockfd = make_connection(PORTNUM);
    char buffer[100];
    ssize_t received = recv(sockfd, buffer, 100,0);
    if (received == -1) {
        perror("recv");
        exit(errno);
    } else if (received == 0) {
        printf("No reply\n");
        close(sockfd);
        return EXIT_FAILURE;
    } else if (received != sizeof(int64_t)) {
        printf("Reply is not an id\n");
        close(sockfd);
        return EXIT_FAILURE;
    }
    uint64_t* reply = (uint64_t*) buffer;
    // do not need to verify all different because tst/hash.cpp
    assert(*reply);
    close(sockfd);
    return EXIT_SUCCESS;
}
#define fork_split() \
    do {\
        pid_t pid = fork();

#define fork_join() \
        if (pid) { \
            int c_ret;\
            waitpid(pid,&c_ret,0); \
            ret =  ret || c_ret; \
        } else {\
            return ret;   \
        }\
    } while(0);


int main(int argc, char* argv[],char*envp[]) {
    pid_t spid = fork();
    if (spid) {
        // parent

        // wait for server to be listening
        wait_server(PORTNUM);

        int client_pid = fork();
        if (client_pid) {
            int cret;
            waitpid(client_pid, &cret,0);
            int sret;
            kill(spid, SIGINT);
            waitpid(spid,&sret,0);
            return sret || cret;
        } else {
            int ret;
            fork_split();
            fork_split();
            fork_split();
            fork_split();
            fork_split();
            fork_split();
            fork_split();
            fork_split();
            ret = client();
            fork_join();
            fork_join();
            fork_join();
            fork_join();
            fork_join();
            fork_join();
            fork_join();
            fork_join();
            return ret;
        }
    } else {
        // child
        return become_id_server(PORTNUM, 1);
    }
}
