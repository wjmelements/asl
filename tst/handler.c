#include "handler.h"
#include "request.h"

#include <assert.h>
#include <errno.h>
#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>

#ifndef NULL
#define NULL 0
#endif

int mkfd() {
    int fd = open("/dev/null",O_WRONLY | O_ASYNC);
    if (fd == -1) {
        perror("open");
        exit(errno);
    }
    return fd;
}
char* request(unsigned male, unsigned looking_male, unsigned looking_female) {
    struct request* ret = malloc(sizeof(struct request));
    ret->age = 16;
    ret->male = male;
    ret->looking_male = looking_male;
    ret->looking_female = looking_female;
    ret->english = 1;
    return bytes_from(ret);
}
extern struct request_node* unmatched_heads[6];
void assert_empty(void) {
    for (int i = 0; i < 6; i++) {
        assert(unmatched_heads[i] == NULL);
    }
}
const int m = 1;
const int f = 2;
void handler_test(void) {
    assert_empty();
    handle(request(m,1,0),mkfd());
    handle(request(m,1,0),mkfd());
    assert_empty();
    handle(request(f,0,1),mkfd());
    handle(request(f,0,1),mkfd());
    assert_empty();
}
int main() {
    handler_init();
    handler_test();
    handler_shutdown(0);
}
