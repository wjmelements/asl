#include "reply.h"
#include "request.h"
#include "tst/common.h"

#include <arpa/inet.h>
#include <assert.h>
#include <errno.h>
#include <sched.h>
#include <stdio.h>
#include <string.h>
#include <sys/wait.h>
#include <unistd.h>

int VERBOSE = 0;

int become_id_server(uint16_t port, int use_valgrind) {
    char* valgrind = "/usr/bin/valgrind";
    char* server = "bin/id_server";
    char port_num[500];
    if (snprintf(port_num, 500, "%u", port) < 0) {
        perror("sprintf");
        exit(errno);
    }
    if (use_valgrind) {
        char* null_argv[5] = {
            valgrind,
            server,
            "-p",
            port_num,
            NULL
        };
        execv(valgrind, null_argv);
    } else {
        char* null_argv[5] = {
            server,
            "-p",
            port_num,
            NULL
        };
        execv(server, null_argv);
    }
    perror("execve");
    return errno;
}

int become_pair_server(uint16_t port) {
    char* server = "bin/pair_server";
    char* valgrind = "/usr/bin/valgrind";
    char port_num[500];
    if (snprintf(port_num, 500, "%u", port) < 0) {
        perror("sprintf");
        exit(errno);
    }
    char* null_argv[8] = {
        valgrind,
        "--leak-check=full",
        server,
        "-p",
        port_num,
        NULL
    };
    execv(valgrind, null_argv);
    perror("execv");
    return errno;
}

void wait_server(uint16_t port) {
    // FIXME if the address is already in use, this method will return even if there isn't a server
    char cmd [100];
    if (sprintf(cmd, "netstat -tln 2>/dev/null | grep ':%u' > /dev/null", port) == -1) {
        perror("sprintf");
    }
    while (WEXITSTATUS(system(cmd)) == 1) {
        sched_yield();
    }
}
int make_connection(uint16_t port) {
    int sockfd = socket(AF_INET, SOCK_STREAM, 0);
    if (sockfd == -1) {
        perror("socket");
        exit(errno);
    }
    struct sockaddr_in dest;
    memset(&dest,0,sizeof(dest));
    dest.sin_family = AF_INET;
    dest.sin_port = htons(port);
    if (connect(sockfd,(struct sockaddr*)&dest,sizeof(struct sockaddr)) == -1) {
        perror("connect");
        exit(errno);
    }
    return sockfd;
}
void send_dummy_object(int sockfd, struct request* req) {
    // TODO test not sending object
    char buffer[100];
    assert(req->obj_size && req->obj_size < 100);
    if (send(sockfd, buffer, req->obj_size, 0) == -1) {
        perror("send");
        exit(errno);
    }
}
void send_request_size(int sockfd, struct request* req, size_t len) {
    if (send(sockfd,req,len,0) == -1) {
        perror("send");
        exit(errno);
    }
}

void send_password(int sockfd, struct request* req) {
    if (send(sockfd, req->password, req->password_len, 0) == -1) {
        perror("send password");
        exit(errno);
    }
}

void send_request(int sockfd, struct request* req)  {
    send_request_size(sockfd, req, sizeof(struct request));
    if (meet(req->type) == FRIEND && action(req->type) == SEND) {
        send_password(sockfd, req);
    }
}

uint16_t recv_object(int sockfd, uint16_t obj_size) {
    char buf[obj_size];
    size_t recvd = recv(sockfd, buf, obj_size * 2, 0);
    if (recvd == -1) {
        close(sockfd);
        perror("recv");
        exit(errno);
    } else if (recvd == 0) {
        close(sockfd);
        fprintf(stderr, "No object\n");
        exit(EXIT_FAILURE);
    }
    return recvd;
}

void cancel_request(struct request* req, int64_t id, meet_type meet) {
    happy_request(req, id, meet);
    req->type = request_type_from(CANCEL, meet);
    req->obj_size = 0;
}

void happy_request(struct request* req, int64_t id, meet_type meet) {
    memset(req,0,sizeof(struct request));
    req->type = request_type_from(SEND, meet);
    if (meet == MATCH) {
        req->age = 18;
        req->looking_male = 1;
        req->male = 1;
        req->iso_en= 1;
    } else { // meet == FRIEND
        req->password = "happy_request_default";
        req->password_len = strlen(req->password);
    }
    req->id = id;
    req->obj_size = 10;
}
void sad_request(struct request* req, meet_type meet) {
    // TODO try other invalid requests
    memset(req,0,sizeof(struct request));
    if (meet == MATCH) {
        req->age = 18;
        req->male = 1;
        req->iso_en= 1;
    } else { // meet == FRIEND
        req->password = "";
        req->password_len = 0;
    }
}

void answer_request(struct request* req, int64_t id, meet_type meet) {
    happy_request(req, id, meet);
    req->type = request_type_from(ANSWER, meet);
}

char* get_password(uint16_t* password_len) {
    int fds[2];
    if (pipe(fds)) {
        perror("pipe");
        exit(errno);
    }
    char* buffer = malloc(50);
    *password_len = 8;
    int pid = fork();
    if (pid) {
        // parent: me
        size_t so_far = 0;
        while (so_far < *password_len) {
            ssize_t now = read(fds[0], buffer + so_far, *password_len - so_far);
            if (now == -1) {
                perror("read");
                exit(errno);
            } else if (now == 0) {
                break;
            }
            so_far += now;
            sched_yield();
        }
        *password_len = so_far;
        close(fds[0]);
    } else {
        // child: pwgen
        fclose(stdout);
        int out = dup2(fds[1], /*stdout*/1);
        if (out == -1) {
            perror("dup2");
            exit(errno);
        }
        close(fds[1]);
        char* program = "/usr/bin/pwgen";
        char* null_argv[4] = {
            program,
            "-s",
            NULL
        };
        execv(program, null_argv);
        perror("execv");
    }
    return buffer;
}

int cancel_client(uint16_t id_port, uint16_t ps_port, meet_type meet) {
    int64_t id = id_from_server(id_port);
    int psfd = make_connection(ps_port);
    struct request req;
    // make a valid request that will not meet any other requests
    happy_request(&req, id, meet);
    if (meet == FRIEND) {
        req.password = get_password(&(req.password_len));
    } else {
        req.iso_en = 0;
        req.iso_es = 1;
        req.male = 1;
        req.looking_male = 0;
        req.looking_female = 1;
    }
    send_request(psfd, &req);
    send_dummy_object(psfd, &req);
    char canc_buffer[100];
    struct reply* canc_reply;
    do {
        int cancfd = make_connection(ps_port);
        struct request can_req;
        cancel_request(&can_req, id, meet);
        can_req.password = req.password;
        can_req.password_len = req.password_len;
        send_request(cancfd, &can_req);
        ssize_t received = recv(cancfd, canc_buffer, sizeof(struct reply), 0);
        if (received == -1) {
            perror("recv:cancfd");
            close(psfd);
            close(cancfd);
            exit(errno);
        } else if (received == 0) {
            fprintf(stderr, "No reply");
            close(psfd);
            close(cancfd);
            return EXIT_FAILURE;
        }
        canc_reply = (struct reply*) canc_buffer;
        close(cancfd);
    } while (!canc_reply->success && canc_reply->reason == not_canceled);
    char buffer[100];
    ssize_t received = recv(psfd, buffer, sizeof(struct reply),0);
    if (received == -1) {
        perror("recv:psfd");
        close(psfd);
        exit(errno);
    } else if (received == 0) {
        fprintf(stderr, "No reply");
        close(psfd);
        return EXIT_FAILURE;
    }
    struct reply* reply = (struct reply*) buffer;
    assert(!reply->success);
    assert(reply->reason == canceled);
    assert(canc_reply->success);
    assert(canc_reply->id == req.id);
    close(psfd);
    return EXIT_SUCCESS;
}

int64_t id_from_server(uint16_t id_port) {
    int idfd = make_connection(id_port);
    char buffer[100];
    ssize_t received = recv(idfd, buffer, 100,0);
    if (received == -1) {
        perror("recv");
        exit(errno);
    } else if (received == 0) {
        fprintf(stderr, "No reply\n");
        close(idfd);
        return EXIT_FAILURE;
    } else if (received != sizeof(int64_t)) {
        fprintf(stderr, "Reply is not an id\n");
        close(idfd);
        return EXIT_FAILURE;
    }
    int64_t id = *(int64_t*) buffer;
    close(idfd);
    return id;
}

int happy_client(uint16_t id_port, uint16_t ps_port, meet_type meet) {
    int64_t id = id_from_server(id_port);
    int psfd = make_connection(ps_port);
    struct request req;
    happy_request(&req, id, meet);
    send_request(psfd, &req);
    send_dummy_object(psfd, &req);
    char buffer[100];
    ssize_t received = recv(psfd, buffer, sizeof(struct reply),0);
    if (received == -1) {
        perror("recv");
        close(psfd);
        exit(errno);
    } else if (received == 0) {
        fprintf(stderr, "No reply");
        close(psfd);
        return EXIT_FAILURE;
    }
    struct reply* reply = (struct reply*) buffer;
    assert(reply->success);
    assert(reply->id != req.id);
    if (meet == MATCH) {
        assert(reply->male ? req.looking_male : req.looking_female);
    }
    assert(recv_object(psfd, reply->obj_size) == reply->obj_size);
    close(psfd);
    if (req.id > reply->id) {
        // am callee
        int answerfd = make_connection(ps_port);
        answer_request(&req, req.id, meet);
        send_request(answerfd, &req);
        send_dummy_object(answerfd, &req);
        ssize_t received = recv(answerfd, buffer, sizeof(struct reply),0);
        if (received == -1) {
            perror("recv");
            exit(errno);
        } else if (received == 0) {
            fprintf(stderr, "No reply");
            close(answerfd);
            return EXIT_FAILURE;
        }
        assert(received == sizeof(struct reply));
        reply = (struct reply*) buffer;
        assert(reply->success);
        assert(reply->obj_size == 0);
        close(answerfd);
    } else {
        // am caller
    }
    return EXIT_SUCCESS;
}
