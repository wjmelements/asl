#ifndef _POSIX_SOURCE
#define _POSIX_SOURCE
#endif

#include <assert.h>
#include <errno.h>
#include <signal.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/socket.h>
#include <sys/wait.h>
#include <unistd.h>

#include "reply.h"
#include "request.h"
#include "tst/common.h"

// port number for pair_server
#define IDPORTNUM 3132
#define PSPORTNUM 3133

/*
    This test suite handles malicious or malconstructed cases and ensures
    desirable behaviour
*/

int hangup_client() {
    int sockfd = make_connection(PSPORTNUM);
    return EXIT_SUCCESS;
}

int no_passwd_client() {
    int sockfd = make_connection(PSPORTNUM);
    struct request req;
    happy_request(&req, /*id */ 1, FRIEND);
    send_request_size(sockfd, &req, sizeof(struct request));
    return EXIT_SUCCESS;
}

int no_obj_client() {
    int sockfd = make_connection(PSPORTNUM);
    struct request req;
    happy_request(&req, /* id */ 1, MATCH);
    send_request(sockfd, &req);
    return EXIT_SUCCESS;
}

int no_friend_obj_client() {
    int sockfd = make_connection(PSPORTNUM);
    struct request req;
    happy_request(&req, /* id */ 1, FRIEND);
    send_request(sockfd, &req);
    exit(EXIT_SUCCESS);
}

int invalid_reqtype_client() {
    int sockfd = make_connection(PSPORTNUM);
    struct request req;
    happy_request(&req, /*id */ 1, MATCH);
    req.type = (ACTION_BITMASK | MEET_BITMASK) + 1;
    send_request(sockfd, &req);
    char buffer[100];
    ssize_t received = recv(sockfd, buffer, 100,0);
    if (received == -1) {
        perror("recv");
        exit(errno);
    } else if (received == 0) {
        printf("No reply\n");
        close(sockfd);
        return EXIT_FAILURE;
    }
    struct reply* reply = (struct reply*) buffer;
    assert(!reply->success);
    assert(reply->reason == bad_request);
    close(sockfd);
    return EXIT_SUCCESS;
}

int noobj_nohang_client() {
    int sockfd = make_connection(PSPORTNUM);
    struct request req;
    happy_request(&req, /*id */ 1, MATCH);
    send_request(sockfd, &req);
    char buffer[100];
    ssize_t received = recv(sockfd, buffer, 100,0);
    if (received == -1) {
        perror("recv");
        exit(errno);
    } else if (received == 0) {
        printf("No reply\n");
        close(sockfd);
        return EXIT_FAILURE;
    }
    struct reply* reply = (struct reply*) buffer;
    assert(!reply->success);
    assert(reply->reason == request_timeout);
    close(sockfd);
    return EXIT_SUCCESS;
}

// sends too little
int small_client() {
    int sockfd = make_connection(PSPORTNUM);
    struct request req;
    happy_request(&req, /* id */ 1, MATCH);
    send_request_size(sockfd, &req, sizeof(struct request)-1);
    send_dummy_object(sockfd, &req);
    char buffer[100];
    ssize_t received = recv(sockfd, buffer, 100,0);
    if (received == -1) {
        perror("recv");
        exit(errno);
    } else if (received == 0) {
        printf("No reply\n");
        close(sockfd);
        return EXIT_FAILURE;
    }
    struct reply* reply = (struct reply*) buffer;
    assert(!reply->success);
    assert(reply->reason == bad_request);
    close(sockfd);
    return EXIT_SUCCESS;
}
// sends too much
int big_client() {
    int sockfd = make_connection(PSPORTNUM);
    struct request req;
    happy_request(&req, /* id */ 1, MATCH);
    send_request_size(sockfd, &req, sizeof(struct request)+1);
    send_dummy_object(sockfd, &req);
    char buffer[100];
    ssize_t received = recv(sockfd, buffer, 100,0);
    if (received == -1) {
        perror("recv");
        exit(errno);
    } else if (received == 0) {
        printf("No reply\n");
        close(sockfd);
        return EXIT_FAILURE;
    }
    struct reply* reply = (struct reply*) buffer;
    assert(!reply->success);
    close(sockfd);
    return EXIT_SUCCESS;
}
// sends invalid request
int invalid_client() {
    int sockfd = make_connection(PSPORTNUM);
    struct request req;
    sad_request(&req, MATCH);
    send_request(sockfd, &req);
    char buffer[100];
    ssize_t received = recv(sockfd, buffer, 100,0);
    if (received == -1) {
        perror("recv");
        exit(errno);
    } else if (received == 0) {
        printf("No reply\n");
        close(sockfd);
        return EXIT_FAILURE;
    }
    struct reply* reply = (struct reply*) buffer;
    assert(!reply->success);
    assert(reply->reason == bad_request);
    close(sockfd);
    return EXIT_SUCCESS;
}
// sends no request
int silent_client() {
    int sockfd = make_connection(PSPORTNUM);
    char buffer[100];
    ssize_t received = recv(sockfd, buffer, 100,0);
    if (received == -1) {
        perror("recv");
        exit(errno);
    } else if (received == 0) {
        printf("No reply\n");
        close(sockfd);
        return EXIT_FAILURE;
    }
    struct reply* reply = (struct reply*) buffer;
    assert(!reply->success);
    assert(reply->reason == request_timeout);
    close(sockfd);
    return EXIT_SUCCESS;
}
// receives no reply
int deaf_client() {
    int sockfd = make_connection(PSPORTNUM);
    struct request req;
    send_request(sockfd, &req);
    close(sockfd);
    return EXIT_SUCCESS;
}

// sends two requests
int double_client() {
    int sockfd = make_connection(PSPORTNUM);
    struct request req;
    happy_request(&req, /* id */ 1, MATCH);
    send_request(sockfd, &req);
    send_dummy_object(sockfd, &req);
    send_request(sockfd, &req);
    send_dummy_object(sockfd, &req);
    char buffer[100];
    ssize_t received = recv(sockfd, buffer, 100,0);
    if (received == -1) {
        perror("recv");
        exit(errno);
    } else if (received == 0) {
        printf("No reply\n");
        close(sockfd);
        return EXIT_FAILURE;
    }
    assert(received == sizeof(struct reply));
    struct reply* reply = (struct reply*) buffer;
    assert(!reply->success);
    assert(reply->reason == bad_request);
    close(sockfd);
    return EXIT_SUCCESS;
}

#define fork_split() \
    do {\
        pid_t pid = fork();\
        if (pid == -1) {\
            perror("fork");\
            exit(errno);\
        }\

#define fork_join() \
        if (pid) { \
            int c_ret;\
            waitpid(pid,&c_ret,0); \
            mret =  mret || c_ret; \
        } else {\
            return mret;   \
        }\
    } while(0);

int do_client(unsigned int todo) {
    switch(todo){
    case 0: return no_obj_client();
    case 1: return hangup_client();
    case 2: return small_client();
    case 3: return big_client();
    case 4: return invalid_client();
    case 5: return deaf_client();
    case 6: return silent_client();
    case 7: return double_client();
    case 8: return no_passwd_client();
    case 9: return no_friend_obj_client();
    case 10: return noobj_nohang_client();
    case 11: return invalid_reqtype_client();
    case 12:
    case 13:
    case 14:
    case 15: return happy_client(IDPORTNUM, PSPORTNUM, MATCH);
    default:
        fprintf(stderr, "No client to run:%u\n",todo);
    }
}
#define fork_add()\
    fork_split(); \
    mine *= 2;\
    if (pid) {\
        mine++;\
    }

// calls all of the above
int all_clients() {
    int mret;
    unsigned int mine = 0;
    fork_add();
    fork_add();
    fork_add();
    fork_add();
    mret = do_client(mine);
    fork_join();
    fork_join();
    fork_join();
    fork_join();
    return mret;
}

int main(int argc, char* argv[],char*envp[]) {
    pid_t spid = fork();
    if (spid) {
        // parent
        pid_t idpid = fork();
        if (idpid) {
            // wait for server to be listening
            wait_server(IDPORTNUM);
            wait_server(PSPORTNUM);

            int client_pid = fork();
            if (client_pid) {
                int cret;
                waitpid(client_pid, &cret,0);
                int sret;
                int idret;
                kill(spid, SIGINT);
                kill(idpid, SIGINT);
                waitpid(spid,&sret,0);
                waitpid(spid,&idret,0);
                return sret || cret || idret;
            } else {
                int mret;
                fork_split();
                fork_split();
                fork_split();
                fork_split();
                all_clients();
                fork_join();
                fork_join();
                fork_join();
                fork_join();
                return mret;
            }
        } else {
            return become_id_server(IDPORTNUM, 0);
        }
    } else {
        // child
        return become_pair_server(PSPORTNUM);
    }
}
