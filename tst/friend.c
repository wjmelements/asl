#include <assert.h>
#include <signal.h>
#include <stdio.h>
#include <stdint.h>
#include <string.h>

#include "friend.h"
#include "request.h"
#include "tst/common.h"

#define NUM_SAMPLES 10
#define NUM_REQUESTS 50

#define assert__(x) for ( ; !(x) ; assert(x) )

struct sample_password {
    struct request_info* info;
    char* password;
};

static struct sample_password passwords[NUM_SAMPLES] = 
  { {NULL, "hello"},
    {NULL, "hellos"},
    {NULL, "aՀայերեն լեզու, I don't even know what that means"},
    {NULL, "  "},
    {NULL, "&*^%\t\nB!~`0-=+XZ<>/"},
    {NULL, "43432142"},
    {NULL, "FALCON PUNCH"},
    {NULL, "Daenerys Stormborn, of House Targaryen, rightful heir to the Iron Throne, Queen of the Seven Kingdoms of Westeros, the Rhoynar, and the First Men, the Mother of Dragons, the Khaleesi of the Great Grass Sea, the Unburnt, and Breaker of Chains"},
    {NULL, "অসমীয়া",},
    {NULL, "\t"}
  };

static const int samples_used[NUM_REQUESTS] =
  {0,0,1,2,1,3,2,0,4,0,
   5,6,7,4,5,8,2,3,0,1,
   0,2,5,9,8,1,2,4,8,6,
   0,5,3,2,7,6,3,8,9,1,
   0,6,6,5,5,3,3,3,3,3};

void init_password(struct sample_password* sample, char* password) {
    sample->info = NULL;
    sample->password = password;
}

void request_init(struct request_info* this, char* password, uint64_t id) {
    struct request* request = malloc(sizeof(*request));
    happy_request(request, /* id */1, FRIEND);
    request->password = password;
    request->password_len = strlen(password);
    request->id = id;
    uint64_t obj_size = 40;
    request->obj_size = 40;
    this->request = request;
    this->obj = malloc(obj_size);
    this->fd = 0;
}
struct request_info* request_new(char* password, uint64_t id) {
    struct request_info* ret = malloc(sizeof(*ret));
    request_init(ret, password, id);
    return ret;
}
void request_cleanup(struct request_info* this) {
    free(this->obj);
    free(this->request);
}
void request_delete(struct request_info* this) {
    request_cleanup(this);
    free(this);
}

void assert_correct_friend(int id) {
    int i = samples_used[id];
    struct request_info* one = request_new(passwords[i].password, id);
    assert__(!friend_cancel(one->request)) {
        fprintf(stderr, "New request prompted cancel\n");
    }
    struct request_info* two = friend_search(one);
    assert__(two == passwords[i].info) {
        fprintf(stderr, "Non-matching passwords: %s and %s\n",
          two->request->password,
          passwords[i].info->request->password);
    }
    if (two) {
        assert__(!friend_cancel(one->request)) {
            fprintf(stderr, "New request matched prompted cancel\n");
        }
        assert__(!friend_cancel(two->request)) {
            fprintf(stderr, "Request recovered prompted cancel\n");
        }
        request_delete(one);
        request_delete(two);
        passwords[i].info = NULL;
    } else {
        passwords[i].info = one;
    }
}


int main() {
    // init
    friend_init();
    // basic test
    for (int64_t id = 0; id < NUM_REQUESTS; id++)
        assert_correct_friend(id);
    // shutdown
    // size_t size;
    // int* fds = friend_shutdown(SIGKILL,&size);
    size_t num_left = 0;
    for (int k = 0; k < NUM_SAMPLES; k++) {
        if (passwords[k].info) {
            struct request_info* req = friend_cancel(passwords[k].info->request);
            assert(req == passwords[k].info);
            struct request_info* replace = request_new(req->request->password, NUM_REQUESTS + k);
            assert(!friend_search(replace)); // would find a match if cancel failed
            request_delete(req);
            passwords[k].info = replace;
            num_left++;
        }
    }
    assert__(num_left > 0) {
        fprintf(stderr, "No unmatched requests left to test cancellation\n");
    }

    size_t size;
    int* fds = friend_shutdown(SIGKILL, &size);
    assert(num_left == size);
    for (int k = 0; k < NUM_SAMPLES; k++) {
        if (passwords[k].info)
            request_delete(passwords[k].info);
    }
    free(fds);
    return 0;
}
