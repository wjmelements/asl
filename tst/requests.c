#include "request.h"

#include <assert.h>
#include <string.h>

// tests both because relationship should be reflexive
#define assert_compat(a,b) assert(requests_compatible(a,b)); assert(requests_compatible(b,a))
#define assert_nocompat(a,b) assert(!requests_compatible(a,b)); assert(!requests_compatible(b,a))

void test_age() {
    struct request one;
    struct request two;
    memset(&one,0,sizeof(struct request));
    memset(&two,0,sizeof(struct request));
    one.type = request_type_from(SEND, MATCH);
    two.type = request_type_from(SEND, MATCH);
    one.male = 1;
    two.male = 1;
    one.looking_male = 1;
    two.looking_male = 1;
    one.iso_en = 1;
    two.iso_en = 1;
    one.age = 14;
    two.age = 14;
    one.obj_size = 10;
    two.obj_size = 10;
    assert(request_valid(&one));
    assert(request_valid(&two));
    assert_compat(&one,&two);
    two.age = 15;
    assert(request_valid(&two));
    assert_compat(&one,&two);
    two.age = 16;
    assert(request_valid(&two));
    assert_nocompat(&one,&two);
    one.age = 15;
    assert(request_valid(&one));
    assert_compat(&one,&two);
    for (uint16_t age1 = 14; age1 <= 120; age1++) {
        one.age = age1;
        for (uint16_t age2 = 14; age2 <= 120; age2++) {
            two.age = age2;
            assert(request_valid(&one));
            assert(request_valid(&two));
            if (age1 < age2) {
                if (age2/2+7 <= age1) {
                    assert_compat(&one,&two);
                } else {
                    assert_nocompat(&one,&two);
                }
            } else {
                if (age1/2+7 <= age2) {
                    assert_compat(&one,&two);
                } else {
                    assert_nocompat(&one,&two);
                }
            }
        }
    }
}
void test_sex() {
    struct request one;
    struct request two;
    memset(&one,0,sizeof(struct request));
    memset(&two,0,sizeof(struct request));
    one.type = request_type_from(SEND, MATCH);
    two.type = request_type_from(SEND, MATCH);
    one.male = 1;
    two.male = 1;
    one.looking_male = 1;
    two.looking_male = 1;
    one.iso_en = 1;
    two.iso_en = 1;
    one.age = 14;
    two.age = 14;
    one.obj_size = 10;
    two.obj_size = 10;
    assert(request_valid(&one));
    assert(request_valid(&two));
    assert_compat(&one,&two);
    one.looking_male = 0;
    assert_nocompat(&one,&two);
    one.looking_female = 1;
    assert_nocompat(&one,&two);
    two.male = 0;
    assert_compat(&one,&two);
    one.looking_male = 1;
    assert_compat(&one,&two);
    one.male = 0;
    assert_nocompat(&one,&two);
    two.looking_female = 1;
    assert_compat(&one,&two);
    one.looking_female = 0;
    assert_nocompat(&one,&two);
}
void test_language() {
    struct request one;
    struct request two;
    memset(&one,0,sizeof(struct request));
    memset(&two,0,sizeof(struct request));
    one.type = request_type_from(SEND, MATCH);
    two.type = request_type_from(SEND, MATCH);
    one.male = 1;
    two.male = 1;
    one.looking_male = 1;
    two.looking_male = 1;
    one.age = 14;
    two.age = 14;
    one.iso_en = 1;
    two.iso_en = 1;
    one.obj_size = 10;
    two.obj_size = 10;
    assert(request_valid(&one));
    assert(request_valid(&two));
    assert_compat(&one,&two);
    one.languages1 = one.languages2 = one.languages3 = 0;
    two.languages1 = two.languages2 = two.languages3 = 0;
    assert_nocompat(&one,&two);
    #define L(name)\
        one.name = 1;\
        assert_nocompat(&one,&two);\
        assert_nocompat(&two,&one);\
        two.name = 1;\
        assert_compat(&one,&two);\
        assert_compat(&two,&one);\
        one.name = 0;\
        assert_nocompat(&one,&two);\
        assert_nocompat(&two,&one);
    LANGUAGES
    #undef L
}

int main() {
    test_age();
    test_sex();
    test_language();
    return 0;
}
