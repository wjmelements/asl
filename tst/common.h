#ifndef asl_tst_common
#define asl_tst_common

#include "request.h"

#include <stdlib.h>

#ifndef NULL
#define NULL 0
#endif

/*
 * If it is in multiple test files, move it here.
 * If it is in a test file multiple times, make it once there.
 */

// create server
int become_id_server(uint16_t port, int valgrind);

// create server
int become_pair_server(uint16_t port);

// wait until a server is listening
void wait_server(uint16_t port);

// make a connection to server, return fd
int make_connection(uint16_t port);

// get ID from a running id server at portnum
int64_t id_from_server(uint16_t port);

// send a request to server , optionally provide size
void send_request(int sockfd, struct request*);
void send_request_size(int sockfd, struct request*, size_t len);

// send object to server
void send_dummy_object(int sockfd, struct request* req);

// receive object after successful reply
uint16_t recv_object(int sockfd, uint16_t obj_size);

// init request structs
void happy_request(struct request*, int64_t id, meet_type);
void sad_request(struct request*, meet_type);
void answer_request(struct request*, int64_t id, meet_type);

// a client that makes a request and follows through
int happy_client(uint16_t id_portnum, uint16_t ps_portnum, meet_type meet);
// a client that makes a request and cancels it
int cancel_client(uint16_t id_portnum, uint16_t ps_portnum, meet_type meet);

#endif
