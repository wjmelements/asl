extern "C" {
    #include "hash.h"
}

#include <assert.h>
#include <unordered_set>
using std::unordered_set;

extern uint64_t hash(uint64_t arg);
#define TEST_SIZE 0xFFFFF
int main() {
    unordered_set<uint64_t> pairs;
    for (uint64_t i = 0; i < TEST_SIZE; i++) {
        pairs.insert(get_hash());
    }
    assert(pairs.size() == TEST_SIZE);
    return 0;
}
