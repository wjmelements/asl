#include <assert.h>
#include <pthread.h>
#include <signal.h>
#include <stdatomic.h>
#include <stdio.h>
#include <stdint.h>

#include "match.h"
#include "tst/common.h"

#define f 0
#define m 1
#define mf 2

void request_init(struct request_node* this, int sex, int looking, uint64_t id) {
    struct request* request = malloc(sizeof(struct request));
    happy_request(request, /* id */1, MATCH);
    if (sex == m) {
        request->male = 1;
    } else {
        request->male = 0;
    }
    switch (looking) {
    case m:
        request->looking_male = 1;
        request->looking_female = 0;
    break;
    case f:
        request->looking_male = 0;
        request->looking_female = 1;
    break;
    case mf:
        request->looking_male = 1;
        request->looking_female = 1;
    break;
    }
    request->id = id;
    uint64_t obj_size = 40;
    request->obj_size = 40;
    this->info.request = request;
    this->info.obj = malloc(obj_size);
    this->next = NULL;
    this->info.fd = 0;
}
struct request_node* request_new(int sex, int looking, uint64_t id) {
    struct request_node* ret = malloc(sizeof(struct request_node));
    request_init(ret, sex, looking, id);
    return ret;
}
void request_cleanup(struct request_node* this) {
    free(this->info.obj);
    free(this->info.request);
}
void request_delete(struct request_node* this) {
    request_cleanup(this);
    free(this);
}
#define assert_matched(sex1, looking1, id1, expected_id) \
do {\
    struct request_node* one = request_new(sex1, looking1, id1);\
    struct request_node* two = match_search(one);\
    assert(two);\
    assert(one != two);\
    assert(two->info.request->id == expected_id);\
    request_delete(one);\
    request_delete(two);\
} while(0)
#define assert_matched_priority(sex1, looking1, id1, expected_id) \
do {\
    struct request_node* one = request_new(sex1, looking1, id1);\
    struct request_node* two = match_prior(one);\
    assert(two);\
    assert(one != two);\
    assert(two->info.request->id == expected_id);\
    request_delete(one);\
    request_delete(two);\
} while(0)
#define assert_not_matched(sex1, looking1, id1) do { \
    struct request_node* one = request_new(sex1, looking1, id1);\
    struct request_node* two = match_search(one);\
    assert(!two);\
} while(0)
#define assert_not_matched_priority(sex1, looking1, id1) do { \
    struct request_node* one = request_new(sex1, looking1, id1);\
    struct request_node* two = match_prior(one);\
    assert(!two);\
} while(0)
#define assert_canceled(sex1, looking1, id1) \
do { \
    struct request_node* one = request_new(sex1, looking1, id1);\
    struct request_node* ret = match_cancel(one->info.request);\
    assert(ret);\
    assert(ret != one);\
    request_delete(one);\
    request_delete(ret);\
} while (0)
#define assert_not_canceled(sex1, looking1, id1)\
do { \
    struct request_node* one = request_new(sex1, looking1, id1);\
    struct request_node* ret = match_cancel(one->info.request);\
    assert(!ret);\
    request_delete(one);\
} while (0)
#define assert_rejected(sex1, looking1, id1)\
do { \
    struct request_node* one = request_new(sex1, looking1, id1);\
    struct request_node* ret = match_search(one);\
    assert(ret == one);\
    request_delete(one);\
} while(0)
void assert_matrix();
void many_unmatched_test();
void para_unmatched_test();
int main() {
    // init
    match_init();
    // basic test
    assert_not_matched(f,m,1);
    assert_matched(m,f,2,1);
    assert_not_matched(m,f,3);
    assert_not_matched(m,f,4);
    assert_matched(f,m,5,3);
    assert_matched(f,mf,6,4);
    assert_not_matched(f,mf,7);
    assert_matched(f,mf,8,7);
    assert_not_matched(m,mf,9);
    assert_matched(m,mf,10,9);
    assert_not_matched(f,m,11);
    assert_not_matched(f,m,12);
    assert_not_matched(m,m,13);
    assert_matched(m,m,14,13);
    assert_matched(m,mf,15,11);
    assert_matched(m,mf,16,12);
    // cancel
    assert_not_matched(m,m,17);
    assert_not_canceled(m,m,16);
    assert_canceled(m,m,17);
    assert_not_canceled(m,m,17);
    assert_not_matched(m,f,18);
    assert_not_matched(m,f,19);
    assert_canceled(m,f,19);
    assert_matched(f,m,20,18);
    // same id
    assert_not_matched(m,m,20);
    assert_rejected(m,m,20);
    assert_matched(m,mf,21,20);
    // priority
    assert_not_matched(m,mf,22);
    assert_matched_priority(m,m,23,22);
    assert_not_matched(m,f,25);
    assert_not_matched(m,f,26);
    assert_not_matched_priority(m,f,27);
    assert_matched(f,m,28,27);
    assert_not_matched_priority(m,f,29);
    assert_matched(f,mf,30,29);
    assert_matched_priority(f,m,31,25);
    assert_matched_priority(f,m,32,26);
    assert_not_matched_priority(f,m,33);
    assert_not_matched_priority(f,m,34);
    assert_not_matched(f,m,35);
    assert_not_matched_priority(f,m,36);
    assert_rejected(f,m,33);
    assert_rejected(f,m,34);
    assert_rejected(f,m,35);
    assert_matched(m,f,37,33);
    assert_matched(m,f,38,34);
    assert_matched(m,f,37,36);
    assert_matched(m,f,37,35);
    assert_not_matched(f,m,38);
    assert_not_matched(f,mf,39);
    assert_not_matched_priority(f,m,40);
    assert_rejected(f,m,40);
    assert_rejected(m,f,40);
    assert_not_matched(m,m,41);
    assert_matched(m,mf,42,40);
    assert_matched(m,m,43,41);
    assert_not_matched_priority(m,m,44);
    assert_canceled(f,m,38);
    assert_canceled(f,mf,39);
    assert_canceled(m,m,44);
    // shutdown
    size_t size;
    int* fds = match_shutdown(SIGKILL,&size);
    assert(size == 0);
    free(fds);
    // rigor tests
    assert_matrix();
    many_unmatched_test();
    para_unmatched_test();
    return 0;
}
// the test matrix checks all possible values of S-Looking
// cells are cells in the test matrix
int check_cell(int sex1, int looking1,
                 int sex2, int looking2) {
    // init
    match_init();
    // test
    struct request_node* one = request_new(sex1, looking1, 1);
    struct request_node* two = request_new(sex2, looking2, 2);
    assert(!match_search(one));
    size_t expected_size;
    if (requests_compatible(one->info.request, two->info.request)) {
        if (match_search(two) != one) {
            fprintf(stderr, "%i %i %i %i\n", sex1, looking1, sex2, looking2);
        }
        expected_size = 0;
        request_delete(one);
        request_delete(two);
    } else {
        if (match_search(two) != NULL) {
            fprintf(stderr, "%i %i %i %i\n", sex1, looking1, sex2, looking2);
        }
        expected_size = 2;
        // match_shutdown should free the nodes and their requests
    }
    // shutdown
    size_t size;
    int* fds = match_shutdown(SIGKILL,&size);
    assert(size == expected_size);
    free(fds);
}
void assert_matrix() {
    for (int sex1 = f; sex1 < mf; sex1++) {
        for (int sex2 = f; sex2 < mf; sex2++) {
            for (int looking1 = f; looking1 <= mf; looking1++) {
                for (int looking2 = f; looking2 <= mf; looking2++) {
                    check_cell(sex1,looking1,sex2,looking2);
                }
            }
        }
    }
}

void many_unmatched_test() {
    // each pass tests a different situation but they initialize the same
    #define NUM_PASSES 3
    for (size_t pass = 0; pass < NUM_PASSES; pass++) {
        match_init();
        const size_t MANY = 5000;
        for (size_t i = 0; i < MANY; i++) {
            assert_not_matched(m, f, i);
        }
        size_t expected_size;
        switch (pass) {
        case 0:
            expected_size = MANY;
            break;
        case 1:
            for (size_t i = 1; i <= MANY; i++) {
                int64_t id = MANY - i;
                assert_canceled(m, f, id);
            }
            expected_size = 0;
            break;
        case 2:
            for (size_t i = 0; i < MANY; i++) {
                int64_t id = i;
                assert_canceled(m, f, id);
            }
            expected_size = 0;
            break;
        default:   
        case NUM_PASSES:
            fprintf(stderr, "Unexpected pass\n");
            assert(0);
            break;
        }
        // shutdown
        size_t size;
        int* fds = match_shutdown(SIGKILL,&size);
        if (size != expected_size) {
            fprintf(stderr, "%lu != %lu\n", size, expected_size);
        }
        assert(size == expected_size);
        free(fds);
    }
    #undef NUM_PASSES
}

volatile int64_t fake_id_server = -50;
void* unmatched_cancel(void* arg) {
    int64_t id = atomic_fetch_add(&fake_id_server, 1);
    assert_not_matched(m, f, id);
    assert_canceled(m, f, id);
    return NULL;
}

void para_unmatched_test() {
    #define NUM_PASSES 4
    // each pass tests a different situation but they initialize the same
    for (size_t pass = 0; pass < NUM_PASSES; pass++) {
        match_init();
        const size_t MANY = 1000;
        pthread_t threads[MANY];
        for (size_t i = 0; i < MANY; i++) {
            pthread_create(threads + i, NULL, unmatched_cancel, NULL);
        }
        for (size_t i = 0; i < MANY; i++) {
            pthread_join(threads[i], NULL);
        }
        // shutdown
        size_t size;
        int* fds = match_shutdown(SIGKILL, &size);
        if (size) {
            fprintf(stderr, "%lu != %u\n", size, 0);
        }
        assert(size == 0);
        free(fds);
    }
    #undef NUM_PASSES
}
