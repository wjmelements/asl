#ifndef _POSIX_SOURCE
#define _POSIX_SOURCE
#endif

#include <arpa/inet.h>
#include <assert.h>
#include <errno.h>
#include <sched.h>
#include <signal.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <unistd.h>

#include "request.h"
#include "reply.h"
#include "tst/common.h"
#define IDPORTNUM 3130
#define PSPORTNUM 3131

#define fork_split() \
    do {\
        pid_t pid = fork();

#define fork_join() \
        if (pid) { \
            int c_ret;\
            waitpid(pid,&c_ret,0); \
            ret =  ret || c_ret; \
        } else {\
            return ret;   \
        }\
    } while(0);


int main(int argc, char* argv[],char*envp[]) {
    pid_t spid = fork();
    if (spid) {
        // parent
        int idpid = fork();
        if (idpid) {
            // wait for server to be listening
            wait_server(IDPORTNUM);
            wait_server(PSPORTNUM);

            int client_pid = fork();
            if (client_pid) {
                int cret;
                int sret;
                int idret;
                waitpid(client_pid, &cret,0);
                kill(spid, SIGINT);
                kill(idpid, SIGINT);
                waitpid(idpid,&idret,0);
                waitpid(spid,&sret,0);
                return sret || idret || cret;
            } else {
                int ret;
                fork_split();
                fork_split();
                fork_split();
                fork_split();
                fork_split();
                fork_split();
                fork_split();
                if (pid) {
                    ret = happy_client(IDPORTNUM, PSPORTNUM, MATCH);
                } else {
                    ret = cancel_client(IDPORTNUM, PSPORTNUM, MATCH);
                }
                fork_join();
                fork_join();
                fork_join();
                fork_join();
                fork_join();
                fork_join();
                fork_join();
                return ret;
            }
        } else {
            // child
            return become_id_server(IDPORTNUM, 0);
        }
    } else {
        // child
        return become_pair_server(PSPORTNUM);
    }
}
