#include "answer.h"
#include "hash.h"

#include <assert.h>
#include <pthread.h>
#include <signal.h>
#include <sched.h>

#define TEST_COUNT 2000

void* answer_event(void* arg) {
    int64_t hash = get_hash();
    sched_yield();
    struct answer_node* dummy = malloc(sizeof(struct answer_node));
    answer_push(hash, dummy);
    sched_yield();
    assert(dummy == answer_pop(hash));
    free(dummy);
    return 0;
}

int main() {
    pthread_t thread[TEST_COUNT];
    for (int i = 0; i < TEST_COUNT; i++) {
        pthread_create(thread+i, NULL, answer_event, NULL);
    }
    for (int i = 0; i < TEST_COUNT; i++) {
        pthread_join(thread[i], NULL);
    }
    size_t num_unanswered_fds;
    // TODO verify answer_shutdown works for fds left behind
    free(answer_shutdown(SIGINT, &num_unanswered_fds));
    assert(num_unanswered_fds == 0);
    return EXIT_SUCCESS;
}
